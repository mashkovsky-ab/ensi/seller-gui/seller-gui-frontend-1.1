import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './index';

interface Profile {
    token: string | null;
}

const uaSlice = createSlice({
    name: 'profile',
    initialState: { token: null } as Profile,
    reducers: {
        setProfile: (state, action: PayloadAction<string | null>) => {
            state.token = action.payload;
        },
    },
});

const { reducer, actions } = uaSlice;
export const { setProfile } = actions;

const user = (state: RootState) => state.profile.token;

export const selectors = { user };

export default reducer;
