import React, { useMemo, useState } from 'react';
import { scale, Layout, Button } from '@greensight/gds';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Table from '@components/Table';
import Popup from '@standart/Popup';
import Select from '@standart/Select';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Legend from '@standart/Legend';
import Pagination from '@standart/Pagination';

const COLUMNS = [
    {
        Header: '№ заказа/№ отправления',
        accessor: 'id',
    },
    {
        Header: 'Дата заказа',
        accessor: 'orderDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Дата доставки',
        accessor: 'deliveryDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Сумма, руб.',
        accessor: 'sum',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Покупатель',
        accessor: 'buyer',
    },
    {
        Header: 'Адрес доставки',
        accessor: 'address',
    },
    {
        Header: 'Дата отгрузки',
        accessor: 'shipmentDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Статус заказа',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Статус отправления',
        accessor: 'shipmentStatus',
        getProps: () => ({ type: 'status' }),
    },
];

const tableItem = (num: number) => {
    return {
        id: `1000${num} / 1000${num}-1`,
        orderDate: new Date(),
        deliveryDate: new Date(),
        sum: 233.43,
        buyer: 'Ларионов Александр Петрович',
        address:
            'Регион: Московская обл, Город: г Солнечногорск, улица: ул Красная, дом: д 120, этаж: 5, квартира: кв 23',
        shipmentDate: new Date(),
        status: 'В обработке',
        shipmentStatus: 'Ожидание отгрузки в ТК',
    };
};

const makeOrders = (len: number) => [...Array(len).keys()].map(el => tableItem(el));

const Orders = () => {
    const data = useMemo(() => makeOrders(3), []);
    const [isOpen, setIsOpen] = useState(false);
    const [activeRow, setActiveRow] = useState<any>(null);
    const [startDateOrder, setStartDateOrder] = React.useState<Date | null>(null);
    const [endDateOrder, setEndDateOrder] = React.useState<Date | null>(null);
    const [startDateDelivery, setStartDateDelivery] = React.useState<Date | null>(null);
    const [endDateDelivery, setEndDateDelivery] = React.useState<Date | null>(null);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    return (
        <>
            <Form
                initialValues={{
                    orderNumber: '',
                    buyer: '',
                    address: '',
                    orderDateFrom: '',
                    orderDateTo: '',
                    deliveryDateFrom: '',
                    deliveryDateTo: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
                css={{ marginBottom: scale(4) }}
            >
                <DatepickerStyles />
                <Layout cols={4} css={{ marginBottom: scale(2) }}>
                    <Layout.Item col={1}>
                        <Form.Field name="orderNumber" label="Номер заказа" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.Field name="buyer" label="Покупатель" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.Field name="address" label="Адрес" />
                    </Layout.Item>
                </Layout>
                <Layout cols={4} css={{ marginBottom: scale(2) }}>
                    <Layout.Item col={1}>
                        <Form.Field name="orderDateFrom">
                            <Legend label="Введите дату от" />
                            <Datepicker
                                selectsStart
                                selected={startDateOrder}
                                startDate={startDateOrder}
                                endDate={endDateOrder}
                                maxDate={endDateOrder}
                                onChange={setStartDateOrder}
                            />
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.Field name="orderDateTo">
                            <Legend label="Введите дату до" />
                            <Datepicker
                                selectsEnd
                                selected={endDateOrder}
                                startDate={startDateOrder}
                                endDate={endDateOrder}
                                minDate={startDateOrder}
                                onChange={setEndDateOrder}
                            />
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.Field name="deliveryDateFrom">
                            <Legend label="Введите дату от" />
                            <Datepicker
                                selectsStart
                                selected={startDateDelivery}
                                startDate={startDateDelivery}
                                endDate={endDateDelivery}
                                maxDate={endDateDelivery}
                                onChange={setStartDateDelivery}
                            />
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.Field name="deliveryDateTo">
                            <Legend label="Введите дату до" />
                            <Datepicker
                                selectsEnd
                                selected={endDateDelivery}
                                startDate={startDateDelivery}
                                endDate={endDateDelivery}
                                minDate={startDateDelivery}
                                onChange={setEndDateDelivery}
                            />
                        </Form.Field>
                    </Layout.Item>
                </Layout>
                <div>
                    <Form.Reset
                        size="sm"
                        theme="secondary"
                        type="button"
                        onClick={() => {
                            setStartDateDelivery(null);
                            setStartDateOrder(null);
                            setEndDateDelivery(null);
                            setEndDateOrder(null);
                        }}
                    >
                        Сбросить
                    </Form.Reset>
                    <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                        Применить
                    </Button>
                </div>
            </Form>

            <Table
                columns={COLUMNS}
                data={data}
                editRow={row => {
                    setIsOpen(true);
                    setActiveRow(row);
                }}
                needCheckboxesCol={false}
                css={{ marginBottom: scale(2) }}
            />
            <Pagination url={pathname} activePage={activePage} pages={7} />

            <Popup
                isOpen={isOpen}
                onRequestClose={() => setIsOpen(false)}
                title={`Редактировать предложение ${activeRow?.id}`}
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        sort: activeRow?.sort,
                        status: activeRow?.status,
                    }}
                    enableReinitialize
                >
                    <Layout cols={2} css={{ marginBottom: scale(2) }}>
                        <Layout.Item col={1}>
                            <Form.Field name="status">
                                <Select
                                    label="В архиве"
                                    defaultIndex={0}
                                    items={[
                                        { value: 'inStock', label: 'В продаже' },
                                        { value: 'inArchive', label: 'В архиве' },
                                        { value: 'notInArchive', label: 'Не в архиве' },
                                    ]}
                                />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="sort" type="number" label="Сортировка" />
                        </Layout.Item>
                    </Layout>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default Orders;
