import React, { useState } from 'react';
import { Layout, scale, Button } from '@greensight/gds';
import { FieldArray, useFormikContext } from 'formik';
import Block from '@components/Block';
import EditIcon from '@svg/tokens/small/edit.svg';
import typography from '@scripts/typography';
import Badge from '@components/Badge';
import YesNoSwitcher from '@standart/YesNoSwitcher';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Select from '@standart/Select';
import PlusIcon from '@svg/plus.svg';
import RemoveIcon from '@svg/tokens/small/trash.svg';
import Legend from '@standart/Legend';
import Textarea from '@standart/Textarea';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';

const FieldArrayBlock = () => {
    const {
        values: { material },
    } = useFormikContext<{ material: string[] }>();

    return (
        <FieldArray
            name="material"
            render={({ push, remove }) => (
                <>
                    <Layout.Item col={2}>
                        <div css={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            Материал
                            <Button hidden theme="outline" size="sm" Icon={PlusIcon} onClick={() => push('')}>
                                добавить
                            </Button>
                        </div>
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <ul>
                            {material.map((i, index) => (
                                <li key={index} css={{ display: 'flex', alignItems: 'center', marginBottom: scale(1) }}>
                                    <Form.Field name={`material[${index}]`} css={{ marginRight: scale(1) }} />
                                    <Button
                                        size="sm"
                                        theme="outline"
                                        hidden
                                        Icon={RemoveIcon}
                                        onClick={() => remove(index)}
                                    >
                                        удалить
                                    </Button>
                                </li>
                            ))}
                        </ul>
                    </Layout.Item>
                </>
            )}
        />
    );
};

const MasterData = () => {
    const [isChangeCharsOpen, setIsChangeCharsOpen] = useState(false);
    const [isContentOpen, setIsContentOpen] = useState(false);
    const [isPropertiesOpen, setIsPropertiesOpen] = useState(false);
    const [isBadgeOpen, setIsBadgeOpen] = useState(false);

    return (
        <>
            <Button size="sm" theme="outline" css={typography('buttonSm')}>
                Убрать в архив
            </Button>
            <Layout cols={4} css={{ marginTop: scale(2) }}>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <p css={typography('h3')}>Характеристики</p>
                            <Button
                                Icon={EditIcon}
                                type="button"
                                theme="ghost"
                                hidden
                                onClick={() => setIsChangeCharsOpen(true)}
                            >
                                редактировать
                            </Button>
                        </Block.Header>
                        <Block.Body>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <th css={{ textAlign: 'left', paddingRight: scale(2) }}>Размер стайлера</th>
                                        <td css={{ textAlign: 'right' }}>Средний</td>
                                    </tr>
                                    <tr>
                                        <th css={{ textAlign: 'left', paddingRight: scale(2) }}>Материал</th>
                                        <td css={{ textAlign: 'right' }}>Дуб</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <p css={typography('h3')}>Состав</p>
                            <Button
                                Icon={EditIcon}
                                type="button"
                                theme="ghost"
                                hidden
                                onClick={() => setIsContentOpen(true)}
                            >
                                редактировать
                            </Button>
                        </Block.Header>
                        <Block.Body>Металл, пластик</Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <p css={typography('h3')}>Свойства товара</p>
                            <Button
                                Icon={EditIcon}
                                type="button"
                                theme="ghost"
                                hidden
                                onClick={() => setIsPropertiesOpen(true)}
                            >
                                редактировать
                            </Button>
                        </Block.Header>
                        <Block.Body>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <th css={{ textAlign: 'left', paddingRight: scale(2) }}>Категория</th>
                                        <td css={{ textAlign: 'right' }}>Стайлеры</td>
                                    </tr>
                                    <tr>
                                        <th css={{ textAlign: 'left', paddingRight: scale(2) }}>Бренд</th>
                                        <td css={{ textAlign: 'right' }}>Ikoo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <p css={typography('h3')}>Шильдики на товаре</p>
                            <Button
                                Icon={EditIcon}
                                type="button"
                                theme="ghost"
                                hidden
                                onClick={() => setIsBadgeOpen(true)}
                            >
                                редактировать
                            </Button>
                        </Block.Header>
                        <Block.Body>
                            <ul
                                css={{
                                    display: 'flex',
                                    flexWrap: 'wrap',
                                    li: { marginRight: scale(1) },
                                }}
                            >
                                <li>
                                    <Badge text="Новинки" />
                                </li>
                                <li>
                                    <Badge text="Хит" />
                                </li>
                            </ul>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
            <Popup
                isOpen={isChangeCharsOpen}
                onRequestClose={() => setIsChangeCharsOpen(false)}
                title="Редактирование характеристик товара"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form onSubmit={values => console.log(values)} initialValues={{ size: '', material: [] }}>
                    <Layout cols={5} css={{ marginBottom: scale(2) }}>
                        <Layout.Item col={2} css={typography('smallBold')}>
                            Характеристика
                        </Layout.Item>
                        <Layout.Item col={3} css={typography('smallBold')}>
                            Значение
                        </Layout.Item>
                        <Layout.Item col={2}>Размер стайлера</Layout.Item>
                        <Layout.Item col={3}>
                            <Form.Field name="size">
                                <Select
                                    items={[
                                        { value: 'sm', label: 'Малый' },
                                        { value: 'md', label: 'Средний' },
                                        { value: 'lg', label: 'Большой' },
                                    ]}
                                />
                            </Form.Field>
                        </Layout.Item>
                        <FieldArrayBlock />
                    </Layout>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsChangeCharsOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isContentOpen}
                onRequestClose={() => setIsContentOpen(false)}
                title="Редактирование состава товара"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form onSubmit={values => console.log(values)} initialValues={{ content: '' }}>
                    <Form.Field name="content">
                        <Legend label="Введите состав" />
                        <Textarea css={{ width: '100%', marginBottom: scale(2) }} />
                    </Form.Field>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsContentOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isPropertiesOpen}
                onRequestClose={() => setIsPropertiesOpen(false)}
                title="Редактирование товара"
                id="-product-edit"
                scrollInside
                popupCss={{
                    maxWidth: 'initial',
                    width: scale(70),
                }}
            >
                <Form
                    onSubmit={values => console.log(values)}
                    initialValues={{
                        name: 'Стайлер для волос IKOO',
                        vendorCode: '1234',
                        status: null,
                        brand: null,
                        category: null,
                        width: '',
                        height: '',
                        weight: '',
                        depth: '',
                        flammable: false,
                        hasPower: false,
                    }}
                >
                    <Layout cols={2} css={{ marginBottom: scale(4) }}>
                        <Layout.Item col={2}>
                            <Form.Field name="name" label="Название товара" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="vendorCode" label="Артикул" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="status" label="Статус проверки">
                                <Select
                                    items={[
                                        { label: 'Согласовано', value: 'agreed' },
                                        { label: 'Не согласовано', value: 'notAgreed' },
                                        { label: 'Отправлено', value: 'sent' },
                                        { label: 'На рассмотрении', value: 'inProcess' },
                                        { label: 'Отклонено', value: 'rejected' },
                                    ]}
                                ></Select>
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <hr />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="brand" label="Бренд">
                                <Select
                                    items={[
                                        { label: 'Бренд 1', value: '1' },
                                        { label: 'Бренд  2', value: '2' },
                                        { label: 'Бренд 3', value: '3' },
                                    ]}
                                ></Select>
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="category" label="Категория">
                                <Select
                                    items={[
                                        { label: 'Категория 1', value: '1' },
                                        { label: 'Категория 2', value: '2' },
                                        { label: 'Категория 3', value: '3' },
                                    ]}
                                ></Select>
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <hr />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field type="number" name="width" label="Ширина, мм" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field type="number" name="height" label="Высота, мм" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field type="number" name="depth" label="Глубина, мм" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field type="number" name="weight" label="Вес, гр" />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <hr />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="flammable">
                                <Legend label="Легковоспламеняющиеся" />
                                <YesNoSwitcher />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="hasPower">
                                <Legend label="В составе есть элемент питания" />
                                <YesNoSwitcher />
                            </Form.Field>
                        </Layout.Item>
                    </Layout>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsPropertiesOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isBadgeOpen}
                onRequestClose={() => setIsBadgeOpen(false)}
                title="Редактирование шильдиков товара"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        badges: [],
                    }}
                >
                    <Form.Field name="badges" css={{ marginBottom: scale(2) }}>
                        <CheckboxGroup label="Шильдики">
                            <Checkbox value="new">Новинки</Checkbox>
                            <Checkbox value="discount">Скидки</Checkbox>
                            <Checkbox value="week">Товар недели</Checkbox>
                            <Checkbox value="hit">Хит</Checkbox>
                        </CheckboxGroup>
                    </Form.Field>

                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsBadgeOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default MasterData;
