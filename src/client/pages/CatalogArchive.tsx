import React, { useState, useMemo } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { useLocation } from 'react-router-dom';
import { FormikValues } from 'formik';

import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import typography from '@scripts/typography';
import { makeRandomData, getRandomItem } from '@scripts/mock';
import { prepareForSelect } from '@scripts/helpers';
import { useURLHelper } from '@scripts/useURLHelper';

import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import MultiSelect from '@standart/MultiSelect';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';

import PlusIcon from '@svg/plus.svg';
import ExportIcon from '@svg/tokens/small/export.svg';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: '',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название и артикул',
        accessor: 'title',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Дата архивации',
        accessor: 'archiveDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Причина',
        accessor: 'reason',
    },
    {
        Header: 'Комментарий',
        accessor: 'comment',
    },
];

const brands = ['lamoda', 'ашан', 'перекресток'];
const brandsForSelect = prepareForSelect(brands);
const categories = ['Шампунь', 'Профессиональные шампуни'];
const categoriesForSelect = prepareForSelect(categories);
const statuses = ['На согласовании', 'В процессе', 'Предзаказ'];
const statusesForSelect = prepareForSelect(statuses);
const reasons = ['Не важно', 'Снято с продажи', 'Нет на складе', 'Снято с производства', 'Другое'];
const reasonsForSelect = prepareForSelect(reasons);

const getTableItem = (id: number) => ({
    id: `100${id}`,
    photo: 'https://picsum.photos/300/300',
    title: getRandomItem([
        ['Резинка для волос женская и детская, прочная и долговечная (браслет пружинка), набор из 3 шт.', 25435],
        ['Стайлер для волос IKOO E-styler Pro White Platina', 12344],
        ['Декоративная игрушка', 112233],
        ['Подушка', 11223344],
        ['Карандаш', 12344321],
        ['Бутылка', 43214321],
        ['Мячик', 12344321],
        ['Шарик', 1234],
        ['Ноутбук', 4311234],
    ]),
    archiveDate: new Date(),
    reason: getRandomItem(reasons),
    comment: getRandomItem(['lorem ipsum dolor', '']),
});

const Filters = ({
    className,
    initialValues,
    onSubmit,
    onReset,
    emptyInitialValues,
}: {
    className?: string;
    onSubmit: (vals: FormikValues) => void;
    onReset: (vals: FormikValues) => void;
    initialValues: FormikValues;
    emptyInitialValues: FormikValues;
}) => {
    const [moreFilters, setMoreFilters] = useState(false);
    const { colors } = useTheme();
    return (
        <Block className={className}>
            <Block.Body>
                <DatepickerStyles />
                <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                    <Block.Body>
                        <Layout cols={12}>
                            <Layout.Item col={4}>
                                <Form.FastField name="title" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.FastField name="archiveDate" label="Дата архивации">
                                    <DatepickerRange />
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.FastField name="reason" label="Причина архивации">
                                    <MultiSelect options={reasonsForSelect} />
                                </Form.FastField>
                            </Layout.Item>
                            {moreFilters ? (
                                <>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="id" label="ID" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="vendorCode" label="Артикул" />
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.FastField name="brand" label="Бренд">
                                            <MultiSelect options={brandsForSelect} />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.FastField name="category" label="Категория">
                                            <MultiSelect options={categoriesForSelect} />
                                        </Form.FastField>
                                    </Layout.Item>
                                </>
                            ) : null}
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>
                            Найдено 135 товаров{' '}
                            <button
                                type="button"
                                css={{ color: colors?.primary, marginLeft: scale(2) }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>{' '}
                        </div>
                        <div>
                            <Form.Reset size="sm" theme="secondary" type="button" initialValues={emptyInitialValues}>
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block.Body>
        </Block>
    );
};

const CatalogArchive = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const data = useMemo(() => makeRandomData(10, getTableItem), []);
    const [ids, setIds] = useState<number[]>([]);

    const emptyInitialValues = {
        title: '',
        archiveDate: [null],
        reason: '',
        id: '',
        vendorCode: '',
        brand: '',
        category: '',
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitialValues);

    return (
        <PageWrapper h1="Архив">
            <>
                <Filters
                    onSubmit={vals => {
                        URLHelper(vals);
                    }}
                    onReset={vals => console.log(vals)}
                    initialValues={initialValues}
                    emptyInitialValues={emptyInitialValues}
                    css={{ marginBottom: scale(3) }}
                />

                <Block>
                    <Block.Header>
                        <div css={{ display: 'flex', alignItems: 'center' }}>
                            <Button
                                theme="primary"
                                size="sm"
                                onClick={() => alert('Тут должен быть экспорт')}
                                Icon={PlusIcon}
                            >
                                Создать товар
                            </Button>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginLeft: scale(2) }}
                                onClick={() => alert('Тут должен быть экспорт')}
                                Icon={ExportIcon}
                            >
                                Экспорт отфильтрованных
                            </Button>
                            <span css={{ marginLeft: scale(2) }}>Выбрано товаров {ids.length}</span>
                            {ids.length > 0 ? (
                                <>
                                    <Button
                                        theme="primary"
                                        size="sm"
                                        css={{ marginLeft: scale(2) }}
                                        onClick={() => alert('Тут должен быть экспорт')}
                                        Icon={ExportIcon}
                                    >
                                        Экспорт выбранных
                                    </Button>
                                </>
                            ) : null}
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} needSettingsColumn={false} onRowSelect={setIds} />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
            </>
        </PageWrapper>
    );
};

export default CatalogArchive;
