import React from 'react';
import { Layout, Button, scale, useTheme } from '@greensight/gds';
import { useFormikContext } from 'formik';

import PageWrapper from '@components/PageWrapper';

import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Block from '@components/Block';

import Radio from '@standart/Radio';
import Form from '@standart/Form';
import Select from '@standart/Select';
import { prepareForSelect } from '@scripts/helpers';

import typography from '@scripts/typography';

import ImportIcon from '@svg/tokens/small/import.svg';
import ExportIcon from '@svg/tokens/small/export.svg';

interface Category {
    name: string;
    subCategories?: Category[];
}

const categories = [
    { name: 'Резинки для волос' },
    { name: 'Стайлеры' },
    { name: 'Ножницы' },
    {
        name: 'Шампуни',
        subCategories: [
            {
                name: 'Профессиональные шампуни',
                subCategories: [{ name: 'Шампуни без парабенов' }, { name: 'Шампуни c парабенами' }],
            },
        ],
    },
    { name: 'Мебель', subCategories: [{ name: 'Диваны' }] },
    { name: 'Товары для дома', subCategories: [{ name: 'Текстиль' }] },
    { name: 'Бытовая техника' },
];

const PrintCategories = ({ categories, margin = 0, ...props }: { categories: Category[]; margin?: number }): any => {
    return categories.map(c => {
        return (
            <React.Fragment key={c.name}>
                <Radio.Item css={{ marginLeft: margin }} value={c.name} {...props}>
                    {c.name}
                </Radio.Item>
                {c.subCategories && c.subCategories.length > 0 ? (
                    <PrintCategories categories={c.subCategories} margin={margin + scale(2)} {...props} />
                ) : null}
            </React.Fragment>
        );
    });
};

const SelectedCategory = () => {
    const {
        values: { category },
    } = useFormikContext();

    return (
        <>
            <p css={{ ...typography('bodyMdBold'), marginBottom: scale(2) }}>
                {category ? `Выбранная категория: "${category}"` : 'Категория не выбрана'}
            </p>
            {category ? (
                <Button size="sm" Icon={ExportIcon}>
                    Скачать шаблон
                </Button>
            ) : null}
        </>
    );
};

const CatalogProductsImport = () => {
    const { colors } = useTheme();

    return (
        <PageWrapper h1="Импорт товаров">
            <Block>
                <Block.Body>
                    <Layout cols={3}>
                        <Layout.Item col={1}>
                            <Form
                                initialValues={{}}
                                onSubmit={() => {
                                    console.log('kek');
                                }}
                            >
                                <Layout cols={1}>
                                    <Layout.Item>
                                        <Form.FastField name="category" label="Категории для загрузки товаров">
                                            <Select items={[]} />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item>
                                        <Form.FastField name="url" label="Ссылка на папку с фотографиями товаров" />
                                    </Layout.Item>
                                    <Layout.Item>
                                        <Form.FastField name="searchField" label="По какому полю искать изображения">
                                            <Select items={prepareForSelect(['Внешний код', 'Артикул'])} />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item>
                                        <FilePondStyles />
                                        <FilePond
                                            maxFiles={1}
                                            acceptedFileTypes={[
                                                'application/vnd.ms-excel',
                                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                            ]}
                                        />
                                    </Layout.Item>
                                    <Layout.Item css={{ textAlign: 'right' }}>
                                        <Button type="submit" size="sm" Icon={ImportIcon}>
                                            Загрузить
                                        </Button>
                                    </Layout.Item>
                                    <Layout.Item>
                                        <div
                                            css={{ background: colors?.warningBg, padding: scale(2), borderRadius: 8 }}
                                        >
                                            <p css={{ marginBottom: scale(2), ...typography('bodyMdBold') }}>
                                                Внимание! Загруженные товары не появятся сразу на витрине!
                                            </p>
                                            <p css={{ marginBottom: scale(2) }}>
                                                Для данных из файла будут созданы новые товары и товарные предложения,
                                                которые отправятся на согласование менеджеру платформы
                                            </p>
                                            <p css={{ marginBottom: scale(1) }}>
                                                Загрузка фотографий для товаров доступна из:
                                            </p>
                                            <ul css={{ li: { listStyle: 'disc' }, paddingLeft: scale(2) }}>
                                                <li>
                                                    общедоступной папки на Яндекс.Диск (работают только ссылки вида
                                                    <a
                                                        href="https://yadi.sk/d/BE9SH7HcQu718w"
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        css={{ color: colors?.primary }}
                                                    >
                                                        https://yadi.sk/d/BE9SH7HcQu718w
                                                    </a>{' '}
                                                    без указания поддиректорий в конце!; такая ссылка не сработает
                                                    <a
                                                        href="https://yadi.sk/d/N6RKyXB8fu9K7A/Authentica"
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        css={{ color: colors?.primary }}
                                                    >
                                                        https://yadi.sk/d/N6RKyXB8fu9K7A/Authentica
                                                    </a>
                                                    !)
                                                </li>
                                            </ul>
                                        </div>
                                    </Layout.Item>
                                </Layout>
                            </Form>
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form initialValues={{ category: '' }} onSubmit={() => console.log('click')}>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <p css={{ ...typography('bodyMdBold'), marginBottom: scale(1) }}>
                                            Выберите категорию для формирования шаблона для загрузки данных о товарах
                                        </p>
                                        <Form.FastField name="category">
                                            <Radio legend="">
                                                <PrintCategories categories={categories} />
                                            </Radio>
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <SelectedCategory />
                                    </Layout.Item>
                                </Layout>
                            </Form>
                        </Layout.Item>
                    </Layout>
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default CatalogProductsImport;
