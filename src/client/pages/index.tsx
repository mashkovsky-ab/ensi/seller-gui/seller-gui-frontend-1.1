import React from 'react';
import loadable from '@loadable/component';
import Loader from '@standart/Loader';

const AsyncPage = loadable((props: any) => import(`./${props.page}`), {
    fallback: (
        <div css={{ width: '100%', height: '60vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Loader />,
        </div>
    ),
});

interface Route {
    /** Page URL */
    path: string[] | string;
    /** Page component name */
    page: string;
    /** Exact */
    exact?: boolean;
    /** Status code (200 if not defined) */
    status?: number;
    /** Data loading function for SSR */
    loadData?: ({ store, req }: any) => Promise<any> | Promise<any>[];
}

const routes: Route[] = [
    {
        path: '/',
        page: 'Home',
        exact: true,
    },
    {
        path: '/requests/check/:id',
        page: 'RequestCheckItem',
    },
    {
        path: '/requests/check',
        page: 'RequestCheck',
    },
    {
        path: '/requests/content/:id',
        page: 'ClaimsContentItem',
    },
    {
        path: '/requests/content',
        exact: true,
        page: 'ClaimsContent',
    },
    {
        path: '/catalog/categories',
        exact: true,
        page: 'CatalogCategories',
    },
    {
        path: '/products/directories/properties/create',
        page: 'ProductPropertyCreate',
    },
    {
        path: '/products/directories/properties',
        page: 'ProductProperties',
    },
    {
        path: '/products/catalog/:id',
        page: 'Product',
    },
    {
        path: '/requests/price-change',
        exact: true,
        page: 'PriceChange',
    },
    {
        path: '/requests/price-change/:id',
        page: 'PriceChangeItem',
    },
    {
        path: '/catalog/products/create',
        exact: true,
        page: 'CatalogProductsCreate',
    },
    {
        path: '/catalog/products/:id',
        page: 'Product',
    },
    {
        path: '/products/products-import',
        page: 'CatalogProductsImport',
    },
    {
        path: '/catalog/products',
        page: 'CatalogProducts',
    },
    {
        path: '/catalog/production',
        page: 'CatalogProduction',
    },
    {
        path: '/catalog/moderation',
        page: 'CatalogModeration',
    },
    {
        path: '/catalog/archive',
        page: 'CatalogArchive',
    },
    {
        path: '/products/offers/:id',
        page: 'Offer',
    },
    {
        path: '/products/offers',
        page: 'Offers',
    },
    {
        path: '/logistics/delivery-services',
        exact: true,
        page: 'DeliveryServices',
    },
    {
        path: '/logistics/delivery-services/:id/',
        page: 'DeliveryServiceDetail',
    },
    {
        path: '/products/variant-groups',
        page: 'VariantGroups',
        exact: true,
    },
    {
        path: '/products/variant-groups/:id',
        page: 'VariantGroup',
    },
    {
        path: '/catalog/brands',
        page: 'Brands',
        exact: true,
    },
    {
        path: '/products/products-import',
        page: 'ProductsImports',
        exact: true,
    },
    {
        path: '/products/amounts-import',
        page: 'ProductsAmountsImport',
    },
    {
        path: '/products/prices-import',
        page: 'ProductsPricesImport',
    },
    {
        path: '/products/imports',
        page: 'Imports',
        exact: true,
    },
    {
        path: '/products/imports/:id',
        page: 'ImportDetail',
    },
    {
        path: '/orders/list',
        page: 'OrderList',
    },
    {
        path: '/marketing/bundles/create',
        page: 'MarketingBundlesCreate',
    },
    {
        path: ['/marketing/discounts/create', '/marketing/discounts/:id/edit'],
        page: 'MarketingDiscountsCreateEdit',
    },
    {
        path: '/marketing/discounts/:id',
        page: 'MarketingBundle',
    },
    {
        path: '/marketing/discounts',
        page: 'MarketingDiscounts',
        exact: true,
    },
    {
        path: '/marketing/bundles',
        page: 'MarketingBundles',
    },
    {
        path: '/stores/seller-stores',
        page: 'SellerStores',
        exact: true,
    },
    {
        path: '/stores/seller-stores/create',
        page: 'SellerStoresCreate',
    },
    {
        path: '/stores/seller-stores/:id',
        page: 'SellerStoresEdit',
    },
    {
        path: '/shipment/list/:id',
        page: 'Shipment',
    },
    {
        path: '/shipment/list',
        page: 'ShipmentList',
    },
    {
        path: '/shipment/problem',
        page: 'ShipmentList',
    },
    {
        path: ['/seller/managers/create', '/seller/managers/:id/edit'],
        page: 'SellerManagersCreate',
    },
    {
        path: '/seller/managers',
        page: 'SellerManagers',
    },
    {
        path: '/reports/dashboard',
        page: 'ReportsDashboard',
    },
    {
        path: '/seller',
        page: 'Seller',
    },
    {
        path: '/shipment/cargo/:id',
        page: 'CargosItem',
    },
    {
        path: '/shipment/cargo',
        page: 'Cargos',
    },
    {
        path: '*',
        page: 'NotFound',
        status: 404,
    },
];

const finalRoutes = routes.map(route => ({
    ...route,
    exact: !!route.exact,
    page: () => <AsyncPage page={route.page} />,
}));

export default finalRoutes;
