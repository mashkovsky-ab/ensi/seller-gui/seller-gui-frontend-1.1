import React, { useMemo, useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { Button, scale } from '@greensight/gds';
import loadable from '@loadable/component';

import { makeRandomData } from '@scripts/mock';

import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import PlusIcon from '@svg/plus.svg';

const Popup = loadable(() => import('@standart/Popup'));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Имя',
        accessor: 'name',
    },
    {
        Header: 'Телефон',
        accessor: 'phone',
    },
    {
        Header: 'Получатель СМС',
        accessor: 'sms',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({
            type: 'status',
        }),
    },
];

const getTableItems = (id: number) => ({
    id,
    name: ['Оболенский Игорь Яковлевич', 'Николай Иванович', 'Василий Петрович'][id],
    phone: '+79259876543',
    sms: 'Да',
    email: 'test@emai.ru',
    status: 'Активный',
});

type Manager = ReturnType<typeof getTableItems>;

const SellerManagers = () => {
    const data = useMemo(() => makeRandomData(3, getTableItems), []);
    const history = useHistory();
    const baseURL = '/seller/managers';

    const [state, setState] = useState<{ isOpen: boolean; data?: Manager }>({
        isOpen: false,
    });
    const close = () => setState({ ...state, isOpen: false });

    return (
        <PageWrapper h1="Менеджеры">
            <Button size="sm" css={{ marginBottom: scale(2) }} Icon={PlusIcon} as={Link} to={`${baseURL}/create`}>
                Добавить менеджера
            </Button>
            <Block>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={data}
                        needCheckboxesCol={false}
                        editRow={row => {
                            history.push(`${baseURL}/${row?.id}/edit`);
                        }}
                        deleteRow={row => setState({ data: row as Manager, isOpen: true })}
                    >
                        <colgroup>
                            <col width="5%" />
                            <col width="20%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="10%" />
                            <col width="5%" />
                        </colgroup>
                    </Table>
                </Block.Body>
            </Block>
            <Popup
                isOpen={state.isOpen}
                onRequestClose={close}
                title={`Вы уверены, что хотите удалить менеджера '${state.data?.name}'?`}
                popupCss={{ minWidth: scale(50) }}
            >
                <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button size="sm" onClick={close} css={{ marginRight: scale(1) }} theme="secondary">
                        Отмена
                    </Button>
                    <Button
                        size="sm"
                        onClick={() => {
                            alert('удалено');
                            close();
                        }}
                    >
                        Удалить
                    </Button>
                </div>
            </Popup>
        </PageWrapper>
    );
};

export default SellerManagers;
