import React from 'react';
import { Redirect } from 'react-router-dom';

const Home = () => <Redirect to={'/shipment/list'} />;

export default Home;
