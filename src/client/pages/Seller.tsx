import React, { useState } from 'react';
import * as Yup from 'yup';
import Block from '@components/Block';
import Badge from '@components/Badge';
import { scale, useTheme, Layout, Button } from '@greensight/gds';
import typography from '@scripts/typography';
import { CSSObject } from '@emotion/core';
import { format } from 'date-fns';
import { STATUSES } from '@scripts/enums';
import Tabs from '@standart/Tabs';
import { Link } from 'react-router-dom';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Table from '@components/Table';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import PageWrapper from '@components/PageWrapper';

const sellerMock = {
    storages: {
        title: 'Адреса складов отгрузки',
        data: '/',
    },
    brands: {
        title: 'Бренды и товарные категории',
        data: '22, 67',
    },
    taxes: {
        title: 'Ставка НДС, тип налогообложения',
        data: '13%',
    },
    conditions: {
        title: 'Коммерческие условия',
        data: '/',
    },
    legalName: {
        title: 'Юридическое наименование организации',
        data: 'Stylers',
    },
    legalAddress: {
        title: 'Юридический адрес',
        data: 'Алабушево',
    },
    address: {
        title: 'Фактический адрес',
        data: 'Алабушево',
    },
    director: {
        title: 'Генеральный директор',
        data: 'Стайлерова Инна Федоровна',
    },
    inn: {
        title: 'ИНН',
        data: '7654367876',
    },
    kpp: {
        title: 'КПП',
        data: '765436787',
    },
    bankNumber: {
        title: 'Номер банковского счета',
        data: '76543678767654367876',
    },
    bank: {
        title: 'Банк',
        data: 'Банк',
    },
    accountNumber: {
        title: 'Номер корреспондентского счета банка',
        data: '76543678767654367876',
    },
    legalBankAddress: {
        title: 'Юридический адрес банка',
        data: 'Москва',
    },
    bik: {
        title: 'Бик банка',
        data: '765436787',
    },
    contractNumber: {
        title: 'Номер договора',
        data: '1234-567-890',
    },
    contractDate: {
        title: 'Дата договора',
        data: format(new Date(), 'dd.MM.yyyy HH:mm'),
    },
    documents: {
        title: 'Документы',
        data: '',
    },
    sellerCommission: '10%',
} as any;

const commissionsMock = [
    {
        id: 1,
        type: 'За бренд',
        commission: '5%',
        brand: 'Бренд: Ikoo',
        date: format(new Date(), 'dd.MM.yyyy HH:mm'),
    },
    {
        id: 2,
        type: 'За категорию',
        commission: '3%',
        brand: 'Категория: Стайлеры',
        date: format(new Date(), 'dd.MM.yyyy HH:mm'),
    },
];

const COLUMNS = [
    {
        Header: 'Тип',
        accessor: 'type',
    },
    {
        Header: 'Комиссия',
        accessor: 'commission',
    },
    {
        Header: 'Бренд/Категория/Товар',
        accessor: 'brand',
    },
    {
        Header: 'Даты активности',
        accessor: 'date',
    },
];

const Seller = () => {
    const { colors } = useTheme();
    const [edit, setEdit] = useState(false);
    const blockBodyStyles: CSSObject = {
        display: 'grid',
        gridTemplateColumns: `auto ${scale(6)}px`,
    };
    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'", gridColumnStart: 'span 2' };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        ...typography('bodySmBold'),
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        textAlign: 'right',
    };
    const itemCss: CSSObject = {
        padding: `${scale(2)}px ${scale(3)}px`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none', paddingBottom: 0 },
    };

    return (
        <PageWrapper h1="Продавец">
            <Layout cols={{ xxxl: 2, sm: 1 }}>
                <Layout.Item cols={1}>
                    <Block css={{ marginBottom: scale(2) }}>
                        <Block.Body css={blockBodyStyles}>
                            <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                <dt css={dtStyles}>Статус:</dt>
                                <dd css={ddStyles}>
                                    <Badge text="Активен" type={STATUSES.SUCCESS} />
                                </dd>
                                <dt css={dtStyles}>Дата регистрации:</dt>
                                <dd css={ddStyles}>{format(new Date(), 'dd.MM.yyyy')}</dd>
                                <dt css={dtStyles}>Ответственный на Маркетплейсе:</dt>
                                <dd css={ddStyles}>Оболенский Игорь Яковлевич</dd>
                            </dl>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
            <Tabs defaultIndex={0}>
                <Tabs.List>
                    <Tabs.Tab>Информация</Tabs.Tab>
                    <Tabs.Tab>Коммерческие условия</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel>
                    <Block>
                        <Block.Body css={{ padding: 0 }}>
                            <Layout gap={0} cols={2}>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.storages.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    <Link css={{ color: colors?.primary }} to={sellerMock.storages.data}>
                                        Склад
                                    </Link>
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.brands.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.brands.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.taxes.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.taxes.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    Коммерческие условия
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    <Link css={{ color: colors?.primary }} to={sellerMock.conditions.data}>
                                        Коммерческие условия
                                    </Link>
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.legalName.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.legalName.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.legalAddress.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.legalAddress.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.address.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.address.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.director.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.director.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.inn.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.inn.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.kpp.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.kpp.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.bankNumber.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.bankNumber.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.bank.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.bank.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.accountNumber.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.accountNumber.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.legalBankAddress.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.legalBankAddress.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.bik.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.bik.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.contractNumber.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.contractNumber.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.contractDate.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.contractDate.data}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    {sellerMock.documents.title}
                                </Layout.Item>
                                <Layout.Item css={itemCss} col={1}>
                                    <FilePondStyles />
                                    <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" />
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <Button theme="primary" size="sm" onClick={() => setEdit(true)}>
                                Редактировать
                            </Button>
                        </Block.Footer>
                    </Block>
                </Tabs.Panel>
                <Tabs.Panel>
                    <Block>
                        <Block.Header>Комиссия продавца: {sellerMock.sellerCommission}</Block.Header>
                        <Block.Body>
                            <Table
                                columns={COLUMNS}
                                data={commissionsMock}
                                needCheckboxesCol={false}
                                needSettingsColumn={false}
                            />
                        </Block.Body>
                    </Block>
                </Tabs.Panel>
            </Tabs>
            <Popup
                isOpen={edit}
                onRequestClose={() => {
                    setEdit(false);
                }}
                title={`Редактировать информацию о продавце`}
                popupCss={{ maxWidth: '100%' }}
                isFullscreen
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                        setEdit(false);
                    }}
                    initialValues={{
                        storages: sellerMock.storages.data,
                        brands: sellerMock.brands.data,
                        taxes: sellerMock.taxes.data,
                        conditions: sellerMock.conditions.data,
                        legalName: sellerMock.legalName.data,
                        legalAddress: sellerMock.legalAddress.data,
                        address: sellerMock.address.data,
                        director: sellerMock.director.data,
                        inn: sellerMock.inn.data,
                        kpp: sellerMock.kpp.data,
                        bankNumber: sellerMock.bankNumber.data,
                        bank: sellerMock.bank.data,
                        accountNumber: sellerMock.accountNumber.data,
                        legalBankAddress: sellerMock.legalBankAddress.data,
                        bik: sellerMock.bik.data,
                        contractNumber: sellerMock.contractNumber.data,
                        contractDate: sellerMock.contractDate.data,
                    }}
                    validationSchema={Yup.object().shape({
                        storages: Yup.string().required('Введите значение'),
                        brands: Yup.string().required('Введите значение'),
                        taxes: Yup.string().required('Введите значение'),
                        conditions: Yup.string().required('Введите значение'),
                        legalName: Yup.string().required('Введите значение'),
                        legalAddress: Yup.string().required('Введите значение'),
                        address: Yup.string().required('Введите значение'),
                        director: Yup.string().required('Введите значение'),
                        inn: Yup.string().required('Введите значение'),
                        kpp: Yup.string().required('Введите значение'),
                        bankNumber: Yup.string().required('Введите значение'),
                        bank: Yup.string().required('Введите значение'),
                        accountNumber: Yup.string().required('Введите значение'),
                        legalBankAddress: Yup.string().required('Введите значение'),
                        bik: Yup.string().required('Введите значение'),
                        contractNumber: Yup.string().required('Введите значение'),
                        contractDate: Yup.string().required('Введите значение'),
                    })}
                >
                    <Layout cols={3} css={{ marginBottom: scale(4) }}>
                        {Object.values(sellerMock)
                            .slice(0, -2)
                            .map((i, index) => {
                                const name = Object.keys(sellerMock)[index];
                                const label = sellerMock[name]?.title;

                                return (
                                    <Layout.Item key={index} col={1}>
                                        <Form.Field name={name} label={label} />
                                    </Layout.Item>
                                );
                            })}
                    </Layout>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setEdit(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default Seller;
