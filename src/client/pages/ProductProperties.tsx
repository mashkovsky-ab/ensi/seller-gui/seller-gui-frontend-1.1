import React, { useState, useMemo, useRef } from 'react';
import { Link } from 'react-router-dom';
import { Button, scale, Layout } from '@greensight/gds';
import Block from '@components/Block';
import useFieldCSS from '@scripts/useFieldCSS';
import typography from '@scripts/typography';
import PlusIcon from '@svg/plus.svg';
import useLinkCSS from '@scripts/useLinkCSS';
import CloseIcon from '@svg/tokens/small/closedCircle.svg';

interface DataItem {
    code: string;
    name: string;
}
const data = [
    { code: 'material', name: 'Материал' },
    { code: 'mechanizm', name: 'Механизм запирания/отпирания канала ствола' },
    { code: 'metall', name: 'Металл' },
    { code: 'mat', name: 'Матовость' },
    { code: '123', name: 'Мерцание' },
    { code: '1', name: 'Маргарин' },
    { code: 'met2all', name: 'Малина' },
    { code: '3', name: 'Магнезия' },
    { code: '4', name: 'Магнит' },
    { code: '5', name: 'Магнитофон' },
    { code: 'new', name: 'Новый' },
    { code: 'size', name: 'Размер' },
    { code: 'color', name: 'цвет' },
    { code: 'char', name: 'Характеристика' },
    { code: 'width', name: 'Ширина' },
    { code: 'height', name: 'Высота' },
    { code: 'depth', name: 'Глубина' },
    { code: 'weight', name: 'Вес' },
    { code: 'tone', name: 'Оттенок' },
];

const ProductProperties = () => {
    const { basicFieldCSS } = useFieldCSS();
    const [search, setSearch] = useState('');
    const linkCSS = useLinkCSS();
    const inputRef = useRef<HTMLInputElement>(null);

    const dict = useMemo(() => {
        const dictTemp = {} as Record<string, DataItem[]>;
        const collator = new Intl.Collator();

        let firstLetter: string;
        let dataCopy = data.slice();
        if (search.length > 0) {
            const regexp = new RegExp(search, 'gim');
            dataCopy = dataCopy.filter(i => regexp.test(i.name));
        }
        dataCopy
            .sort((a, b) => collator.compare(a.name.toLowerCase(), b.name.toLowerCase()))
            .forEach(item => {
                if (
                    item.name.charAt(0).toUpperCase() !== firstLetter &&
                    /[A-ZА-ЯЁ]/.test(item.name.charAt(0).toUpperCase())
                ) {
                    firstLetter = item.name.charAt(0).toUpperCase();
                    dictTemp[firstLetter] = [item];
                } else {
                    dictTemp[firstLetter].push(item);
                }
            });
        return dictTemp;
    }, [search]);

    return (
        <>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>
                    Справочник товарных атрибутов
                </h1>
                <Block>
                    <Block.Header>
                        <form css={{ position: 'relative' }}>
                            <label htmlFor="searchAttr" css={{ marginBottom: scale(1), display: 'inline-block' }}>
                                Поиск атрибута
                            </label>
                            <input
                                ref={inputRef}
                                value={search}
                                onChange={e => setSearch(e.target.value)}
                                type="text"
                                id="searchAttr"
                                css={basicFieldCSS}
                                placeholder="Введите атрибут..."
                            />
                            {search.length > 0 ? (
                                <button
                                    type="reset"
                                    title="Очистить"
                                    css={{
                                        position: 'absolute',
                                        bottom: 0,
                                        right: 0,
                                        height: scale(7, true),
                                        width: scale(7, true),
                                    }}
                                    onClick={() => {
                                        setSearch('');
                                        inputRef?.current?.focus();
                                    }}
                                >
                                    <CloseIcon />
                                </button>
                            ) : null}
                        </form>
                        <Button size="sm" theme="primary" as={Link} to="properties/create" Icon={PlusIcon}>
                            Создать атрибут
                        </Button>
                    </Block.Header>
                    <Block.Body>
                        <Layout auto={scale(20)}>
                            {Object.keys(dict).length > 0 ? (
                                Object.keys(dict).map(letter => (
                                    <Layout.Item key={letter} col={1}>
                                        <p css={{ ...typography('bodyMdBold'), marginBottom: scale(2) }}>
                                            {letter.toUpperCase()}
                                        </p>
                                        <ul css={{ listStyleType: 'dash' }}>
                                            {dict[letter].map(i => (
                                                <li key={i.code} css={{ marginBottom: scale(1) }}>
                                                    <Link
                                                        to={`/products/dictionaries/properties/${i.code}`}
                                                        css={linkCSS}
                                                    >
                                                        {i.name.slice(0, 1).toUpperCase() + i.name.slice(1)}
                                                    </Link>
                                                </li>
                                            ))}
                                        </ul>
                                    </Layout.Item>
                                ))
                            ) : (
                                <p>Ничего не найдено</p>
                            )}
                        </Layout>
                    </Block.Body>
                </Block>
            </main>
        </>
    );
};

export default ProductProperties;
