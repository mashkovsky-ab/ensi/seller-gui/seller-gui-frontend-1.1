import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';
import { useFormikContext, FieldArray, useField } from 'formik';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import Form from '@standart/Form';
import Select from '@standart/Select';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';
import Block from '@components/Block';
import typography from '@scripts/typography';
import TrashIcon from '@svg/tokens/small/trash.svg';
import { debounce } from '@scripts/helpers';
import usePrevious from '@scripts/usePrevious';
import { Categories, categoriesData } from '@scripts/mock';

const attrTypes = [
    { label: 'Строка', value: 'string' },
    { label: 'Целое число', value: 'fractional' },
    { label: 'Дата/время', value: 'date' },
    { label: 'Значение из списка', value: 'list' },
];

const PrintCategories = ({
    categories,
    margin = 0,
    parentCategoryName = [],
    ...props
}: {
    categories: Categories[];
    margin?: number;
    parentCategoryName?: string[];
}): any => {
    const [field] = useField('categories');
    const formCategories = field.value;
    return categories.map(c => {
        return (
            <div key={c.name} css={{ marginBottom: scale(2) }}>
                <Checkbox
                    key={c.name}
                    css={{ marginLeft: margin, marginBottom: scale(2), '>label': { display: 'inline-block' } }}
                    disabled={parentCategoryName.length > 0 && parentCategoryName.some(p => formCategories.includes(p))}
                    value={c.name}
                    {...props}
                >
                    {c.name}
                </Checkbox>
                {c.subCategories && c.subCategories.length > 0 ? (
                    <PrintCategories
                        categories={c.subCategories}
                        margin={margin + scale(2)}
                        parentCategoryName={[...parentCategoryName, c.name]}
                        {...props}
                    />
                ) : null}
            </div>
        );
    });
};

interface AdditionalAttributeItemProps {
    isColorField: boolean;
    index: number;
    remove: (i: number) => void;
}
const AdditionalAttributeItem = ({ isColorField, index, remove }: AdditionalAttributeItemProps) => {
    const {
        setFieldValue,
        values: { additionalAttributes },
    } = useFormikContext<FormValuesTypes>();

    const currentColorValue = additionalAttributes[index].value;
    const prevCurrentValue = usePrevious(currentColorValue);

    useEffect(() => {
        if (prevCurrentValue !== currentColorValue) {
            setColor(currentColorValue);
        }
    }, [prevCurrentValue, currentColorValue]);

    const changeColor = React.useCallback(
        debounce((e: any) => {
            const color = e?.target?.value;
            setFieldValue(`additionalAttributes[${index}].value`, color);
        }, 300),
        [index]
    );
    const [color, setColor] = useState('#000000');

    return (
        <li css={{ marginBottom: scale(2) }}>
            <Layout cols={5}>
                {isColorField ? (
                    <>
                        <Layout.Item col={2}>
                            <Form.Field
                                label={`Возможное значение ${index + 1}`}
                                name={`additionalAttributes[${index}].name`}
                            />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field label="Код цвета" name={`additionalAttributes[${index}].value`} />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <input
                                name={`additionalAttributes[${index}].value`}
                                type="color"
                                value={color}
                                onChange={e => {
                                    changeColor(e);
                                    setColor(e.target.value);
                                }}
                                css={{ width: '100%', height: scale(7, true), marginTop: scale(3) }}
                            />
                        </Layout.Item>
                    </>
                ) : (
                    <Layout.Item col={3}>
                        <Form.Field
                            label={`Возможное значение ${index + 1}`}
                            name={`additionalAttributes[${index}].value`}
                        />
                    </Layout.Item>
                )}
                <Layout.Item col={1}>
                    <Button
                        theme="outline"
                        size="sm"
                        disabled={additionalAttributes.length < 3}
                        title="Удалить атрибут"
                        onClick={() => remove(index)}
                        css={{ marginTop: scale(3) }}
                    >
                        <TrashIcon />
                    </Button>
                </Layout.Item>
            </Layout>
        </li>
    );
};

interface FormValuesTypes {
    attrType: string;
    attrProps: string[];
    productNameForAdmin: string;
    productNameForPublic: string;
    categories: string[];
    additionalAttributes: { name: string; value: string }[];
}

const FormChildren = () => {
    const {
        dirty,
        values: { attrType, attrProps, additionalAttributes },
        setFieldValue,
    } = useFormikContext<FormValuesTypes>();
    const { colors } = useTheme();
    const isColorField = attrProps ? attrProps.some(i => i === 'color') : false;
    useEffect(() => {
        if (attrType !== 'list' && isColorField) {
            setFieldValue(
                'attrProps',
                attrProps.filter(i => i !== 'color')
            );
        }
    }, [attrProps, attrType, isColorField, setFieldValue]);

    return (
        <Block>
            <Block.Body>
                <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Общие параметры товарного атрибута: </h2>
                <Layout cols={2}>
                    <Layout.Item>
                        <Form.Field name="productNameForAdmin" label="Название атрибута для  публичной  части сайта" />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field
                            name="productNameForPublic"
                            label="Название атрибута для административного раздела"
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field name="attrType">
                            <Select label="Тип атрибута" items={attrTypes} />
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field name="attrProps">
                            <CheckboxGroup>
                                <Checkbox
                                    value="color"
                                    checked={attrProps && attrProps.some(i => i === 'color')}
                                    disabled={attrType !== 'list'}
                                >
                                    Атрибут хранит цвет
                                </Checkbox>
                                <Checkbox value="filter">Выводить атрибут в фильтр товаров</Checkbox>
                                <Checkbox value="fewValues">Атрибут хранит несколько значений</Checkbox>
                            </CheckboxGroup>
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item>
                        <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Атрибут актуален для категорий:</h2>
                        <Form.Field name="categories">
                            <PrintCategories categories={categoriesData} />
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item>
                        <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Атрибут может принимать значения:</h2>
                        {attrType === 'list' ? (
                            <>
                                <p css={{ marginBottom: scale(2) }}>
                                    Укажите не менее двух возможных значений для атрибута:
                                </p>
                                <FieldArray
                                    name="additionalAttributes"
                                    render={({ push, remove }) => {
                                        return (
                                            <>
                                                <ul>
                                                    {additionalAttributes.map((atr, index) => (
                                                        <AdditionalAttributeItem
                                                            index={index}
                                                            remove={remove}
                                                            isColorField={isColorField}
                                                            key={index}
                                                        />
                                                    ))}
                                                </ul>
                                                <Button size="sm" onClick={() => push('')}>
                                                    Добавить значение
                                                </Button>
                                            </>
                                        );
                                    }}
                                />
                            </>
                        ) : (
                            <>
                                <p css={{ marginBottom: scale(2), color: colors?.success }}>
                                    Атрибут может принимать любые значения
                                </p>
                                <p css={{ marginBottom: scale(2) }}>
                                    Чтобы установить жестко заданные варианты, выберите тип &ldquo;Значение из
                                    списка&rdquo;
                                </p>
                            </>
                        )}
                    </Layout.Item>
                </Layout>
            </Block.Body>
            <Block.Footer>
                <Button type="submit" disabled={!dirty}>
                    Создать товарный атрибут
                </Button>
            </Block.Footer>
        </Block>
    );
};

const ProductPropertyCreate = () => {
    return (
        <>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>Создание товарного атрибута</h1>
                <Form
                    onSubmit={values => console.log(values)}
                    initialValues={{
                        attrType: '',
                        attrProps: [],
                        productNameForAdmin: '',
                        productNameForPublic: '',
                        categories: [],
                        additionalAttributes: [
                            { name: '', value: '' },
                            { name: '', value: '' },
                        ],
                    }}
                    validationSchema={Yup.object().shape({
                        productNameForAdmin: Yup.string().required('Введите название атрибута'),
                        productNameForPublic: Yup.string().required('Введите название атрибута'),
                        additionalAttributes: Yup.array()
                            .of(
                                Yup.object().shape({
                                    name: Yup.string(),
                                    value: Yup.string().required('Введите значение'),
                                })
                            )
                            .min(2, 'Введите минимум 2 атрибута')
                            .required('Атрибуты  обязательны'),
                    })}
                >
                    <FormChildren />
                </Form>
            </main>
        </>
    );
};

export default ProductPropertyCreate;
