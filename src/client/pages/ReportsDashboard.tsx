import React from 'react';
import { Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

const ReportsDashboard = () => {
    return (
        <PageWrapper h1="Статистика за Май 2021">
            <Layout cols={2}>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <h2 css={typography('h2')}>Товары</h2>
                        </Block.Header>
                        <Block.Body>
                            <table css={{ width: '100%', th: { textAlign: 'left' } }}>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="35%" />
                                    <col width="35%" />
                                </colgroup>
                                <thead>
                                    <th></th>
                                    <th>Количество</th>
                                    <th>Стоимость</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Товаров на витрине</th>
                                        <td>1 ед</td>
                                        <td>500 руб.</td>
                                    </tr>
                                    <tr>
                                        <th>Продано товаров</th>
                                        <td>0 ед.</td>
                                        <td>0 руб.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <h2 css={typography('h2')}>Заказы</h2>
                        </Block.Header>
                        <Block.Body>
                            <table css={{ width: '100%', th: { textAlign: 'left' } }}>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="35%" />
                                    <col width="35%" />
                                </colgroup>
                                <thead>
                                    <th></th>
                                    <th>Количество</th>
                                    <th>Стоимость</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Принято заказов </th>
                                        <td>1 ед</td>
                                        <td>500 руб.</td>
                                    </tr>
                                    <tr>
                                        <th>Доставлено заказов </th>
                                        <td>0 ед.</td>
                                        <td>0 руб.</td>
                                    </tr>
                                    <tr>
                                        <th>Уплачено комиссии </th>
                                        <td colSpan={2}>0 руб</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
        </PageWrapper>
    );
};

export default ReportsDashboard;
