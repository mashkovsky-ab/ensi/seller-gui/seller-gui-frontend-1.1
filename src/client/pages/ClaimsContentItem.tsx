import React, { useState, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import PageWrapper from '@components/PageWrapper';
import { CSSObject } from '@emotion/core';
import { format } from 'date-fns';
import { scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Block from '@components/Block';
import Badge from '@components/Badge';
import Tabs from '@standart/Tabs';
import Textarea from '@standart/Textarea';
import typography from '@scripts/typography';
import { makeClaimsContent } from '@scripts/mock';

const columns = [
    {
        Header: 'ID оффера',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Артикул',
        accessor: 'code',
    },
    {
        Header: 'Бренд',
        accessor: 'brand',
    },
    {
        Header: 'Категория',
        accessor: 'category',
    },
];

const data = [
    {
        id: 526,
        title: 'Стайлер для волос IKOO E-styler Pro Beluga Black	',
        code: 292430,
        brand: 'Ikoo',
        category: 'Стайлеры',
    },
];

const claimItem = makeClaimsContent(1)[0];

const ClaimsContentItem = () => {
    const { id } = useParams<{ id: string }>();
    const { type, status, claimAuthor, shootingType, created } = claimItem;
    const { colors } = useTheme();

    const [showCommentSaving, setShowCommentSaving] = useState(false);

    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    const commentHandler = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setShowCommentSaving(true);
        setTimeout(() => {
            setShowCommentSaving(false);
        }, 1000);
    };

    const tableData = useMemo(() => data, []);
    const tableColumns = useMemo(() => columns, []);
    return (
        <PageWrapper h1={`Заявка на производство контента #${id}`}>
            <main>
                <Layout cols={2} css={{ marginBottom: scale(3) }}>
                    <Layout.Item>
                        <Block>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: '150px 1fr' }}>
                                    <dt css={dtStyles}>Статус</dt>
                                    <dd css={ddStyles}>
                                        <Badge text={status} />
                                    </dd>
                                    <dt css={dtStyles}>ID</dt>
                                    <dd css={ddStyles}>{id}</dd>
                                    <dt css={dtStyles}>Тип заявки</dt>
                                    <dd css={ddStyles}>{type}</dd>
                                    <dt css={dtStyles}>Тип фотосъемки</dt>
                                    <dd css={ddStyles}>{shootingType}</dd>
                                    <dt css={dtStyles}>Автор</dt>
                                    <dd css={ddStyles}>{claimAuthor[0]}</dd>
                                    <dt css={dtStyles}>Дата создания</dt>
                                    <dd css={ddStyles}>{format(new Date(created), 'dd.MM.yyyy')}</dd>
                                    <dt css={dtStyles}>
                                        Комментарий продавца <br />
                                        {showCommentSaving && <Badge text="Сохранено" />}
                                    </dt>
                                    <dd css={ddStyles}>
                                        <Textarea rows={3} onBlur={commentHandler} />
                                    </dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Товары</Tabs.Tab>
                    </Tabs.List>
                    <Block>
                        <Block.Body>
                            <Tabs.Panel>
                                <Table
                                    columns={tableColumns}
                                    data={tableData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                        </Block.Body>
                    </Block>
                </Tabs>
            </main>
        </PageWrapper>
    );
};

export default ClaimsContentItem;
