import React, { useState, useMemo, useCallback } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { Helmet } from 'react-helmet-async';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import Select from '@standart/Select';
import { makeShipments } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import { useLocation } from 'react-router-dom';
import Popup from '@standart/Popup';
import Tooltip from '@standart/Tooltip';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import DatepickerStyles from '@standart/Datepicker/presets';
import Datepicker from '@standart/Datepicker';
import DatepickerRange from '@standart/DatepickerRange';
import Legend from '@standart/Legend';
import CopyIcon from '@svg/tokens/small/copy.svg';
import SettingsIcon from '@svg/tokens/small/settings.svg';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';

import { prepareForSelect } from '@scripts/helpers';
import { STATUSES, STOCKS } from '@scripts/data/different';

const statuses = prepareForSelect(STATUSES);

const stocks = prepareForSelect(STOCKS);

const COLUMNS = [
    {
        Header: '№ заказа',
        accessor: 'id',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Дата заказа',
        accessor: 'dateOrder',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Сумма',
        accessor: 'sum',
    },
    {
        Header: 'Требуемая дата отгрузки',
        accessor: 'dateShipment',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Склад',
        accessor: 'stock',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Кол-во коробок',
        accessor: 'countBox',
    },
    {
        Header: 'Комментарий',
        accessor: 'comment',
    },
];

const changeStatusPopupColumns = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
        getProps: () => ({ type: 'double' }),
    },
];

const ShipmentList = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [startDate, setStartDate] = React.useState<Date | null>(null);
    const [endDate, setEndDate] = React.useState<Date | null>(null);
    const [moreFilters, setMoreFilters] = useState(false);
    const [ids, setIds] = useState<number[]>([]);
    const data = useMemo(() => makeShipments(10), []);

    const onRowSelect = (ids: number[]) => setIds(ids);

    return (
        <>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                <Helmet>
                    <title>Заказы на сборку</title>
                </Helmet>
                <DatepickerStyles />
                <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>Заказы на сборку</h1>

                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            orderDate: '',
                            shipmentDate: '',
                            status: '',
                            stock: '',
                            orderNumber: '',
                            sumFrom: '',
                            sumTo: '',
                            codeERP: '',
                            vendorCode: '',
                            brand: '',
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={12}>
                                <Layout.Item col={3}>
                                    <Form.Field name="orderDate" label="Дата заказа">
                                        <DatepickerRange />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.Field name="shipmentDate">
                                        <Legend label="Требуемая дата отгрузки" />
                                        <Datepicker />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.Field name="status" label="Статус заказа">
                                        <Select items={statuses} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.Field name="stock" label="Склад отгрузки">
                                        <Select items={stocks} />
                                    </Form.Field>
                                </Layout.Item>

                                {moreFilters ? (
                                    <>
                                        <Layout.Item col={2}>
                                            <Form.Field name="orderNumber" label="№ заказа" />
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field
                                                name="sumFrom"
                                                label="Сумма заказа"
                                                type="number"
                                                placeholder="От"
                                            />
                                        </Layout.Item>
                                        <Layout.Item col={2} css={{ marginTop: scale(3) }} align="end">
                                            <Form.Field name="sumTo" type="number" placeholder="До" />
                                        </Layout.Item>

                                        <Layout.Item col={2}>
                                            <p css={{ marginBottom: scale(1) }}>
                                                Код товара из ERP{' '}
                                                <Tooltip
                                                    content="Код из внешней системы, по которому импортируется товар"
                                                    arrow
                                                    maxWidth={scale(30)}
                                                >
                                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                                        <TipIcon />
                                                    </button>
                                                </Tooltip>
                                            </p>
                                            <Form.Field name="codeERP" />
                                        </Layout.Item>

                                        <Layout.Item col={2}>
                                            <p css={{ marginBottom: scale(1) }}>
                                                Артикул{' '}
                                                <Tooltip
                                                    content="Артикул товара в системе iBT"
                                                    arrow
                                                    maxWidth={scale(30)}
                                                >
                                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                                        <TipIcon />
                                                    </button>
                                                </Tooltip>
                                            </p>
                                            <Form.Field name="vendorCode" />
                                        </Layout.Item>
                                        <Layout.Item col={4}>
                                            <Form.Field name="brand">
                                                <p css={{ marginBottom: scale(1) }}>
                                                    Бренд{' '}
                                                    <Tooltip
                                                        content="Будут показаны заказы в которых есть товары указанного бренда"
                                                        arrow
                                                        maxWidth={scale(30)}
                                                    >
                                                        <button type="button" css={{ verticalAlign: 'middle' }}>
                                                            <TipIcon />
                                                        </button>
                                                    </Tooltip>
                                                </p>
                                                <Select
                                                    defaultIndex={0}
                                                    items={[
                                                        { value: '', label: 'Не выбрано' },
                                                        { value: 'loreal', label: 'Loreal' },
                                                        { value: 'BrookBond', label: 'BrookBond' },
                                                    ]}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                    </>
                                ) : null}
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <div css={typography('bodySm')}>
                                Найдено 135 товаров{' '}
                                <button
                                    type="button"
                                    css={{ color: colors?.primary, marginLeft: scale(2) }}
                                    onClick={() => setMoreFilters(!moreFilters)}
                                >
                                    {moreFilters ? 'Меньше' : 'Больше'} фильтров
                                </button>{' '}
                            </div>
                            <div>
                                <Form.Reset
                                    size="sm"
                                    theme="secondary"
                                    type="button"
                                    onClick={() => {
                                        setStartDate(null);
                                        setEndDate(null);
                                    }}
                                >
                                    Сбросить
                                </Form.Reset>
                                <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </Block.Footer>
                    </Form>
                </Block>

                <Block>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            editRow={row => console.log('rowdata', row)}
                            onRowSelect={onRowSelect}
                        />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
            </main>
        </>
    );
};

export default ShipmentList;
