import React, { useMemo, useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { CSSObject } from '@emotion/core';
import { format } from 'date-fns';
import { Button, scale, useTheme, Layout } from '@greensight/gds';

import PageWrapper from '@components/PageWrapper';
import Table, { makeStringRow, makeStringDeleteRow } from '@components/Table';
import headerWithTooltip from '@components/Table/HeaderWithTooltip';
import HeaderDouble from '@components/Table/HeaderDouble';
import Block from '@components/Block';
import Badge from '@components/Badge';

import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Tabs from '@standart/Tabs';
import Tooltip from '@standart/Tooltip';
import Checkbox from '@standart/Checkbox';
import CheckboxGroup from '@standart/CheckboxGroup';
import Select from '@standart/Select';

import { nanoid } from 'nanoid';

import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import ExportIcon from '@svg/tokens/small/export.svg';
import FileIcon from '@svg/tokens/small/file.svg';
import PlusIcon from '@svg/plus.svg';

import typography from '@scripts/typography';
import { formatPrice, prepareForSelect } from '@scripts/helpers';
import { shipmentData, makeShipmentHistory, makeShipmentOrderList } from '@scripts/mock';

export interface Store {
    storeName: string;
    quantity: number;
}

const boxesList = ['Коробка 10х20х30', 'Коробка 20х30х40', 'Коробка 30х40х50'];

const typeBoxes = prepareForSelect(boxesList);

const orderListСolumns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Фото',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: () =>
            HeaderDouble({
                headerText: 'Название',
                smallText: 'Артикул',
            }),
        accessor: 'title',
        getProps: () => ({ type: 'array' }),
    },
    {
        Header: () =>
            HeaderDouble({
                headerText: 'Категория',
                smallText: 'Бренд',
            }),
        accessor: 'category',
        getProps: () => ({ type: 'array' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Цена за единицу без скидки, руб',
                tooltipText: 'Цена товара без скидки за единицу товара',
            }),
        accessor: 'price',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Скидка за единицу',
                tooltipText: 'Величина скидки за единицу товара',
            }),
        accessor: 'discount',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Цена за единицу со скидкой',
                tooltipText: 'Цена товара со всеми скидками за единицу товара',
            }),
        accessor: 'priceWithDiscount',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Сумма без скидки',
                tooltipText: 'Цена товара без скидки с учётом количества',
            }),
        accessor: 'sum',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Скидка',
                tooltipText: 'Величина скидки с учётом количества',
            }),
        accessor: 'sumDiscount',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Сумма со скидкой',
                tooltipText: 'Цена товара со всеми скидками с учётом количества',
            }),
        accessor: 'sumWithDiscount',
        getProps: () => ({ type: 'price' }),
    },
];

const historyСolumns = [
    {
        Header: 'Дата',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
    },
    {
        Header: 'Сущность',
        accessor: 'essense',
    },
    {
        Header: 'Действие',
        accessor: 'action',
    },
];

const shipment = shipmentData;

const Shipment = () => {
    const [isAddingOrderPopupOpen, setAddingOrderPopupOpen] = useState(false);
    const { id } = useParams<{ id: string }>();
    const { weigth, price, discount, created, warehouse, statusText, boxes, quantity } = shipment;
    const { colors } = useTheme();

    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
        textAlign: 'right',
    };

    const newOrderList = useMemo(() => makeShipmentOrderList(3), []);

    const [orderList, setOrderList] = useState(
        boxesList.map(box => {
            return {
                id: nanoid(5),
                box: box,
                products: newOrderList,
            };
        })
    );

    const addBoxHandler = (value: any) => {
        setOrderList([...orderList, { id: nanoid(5), box: value.type, products: [] }]);
    };

    const deleteBoxHandler = (id: string) => {
        const newList = [...orderList].filter(item => item.id !== id);
        setOrderList(newList);
    };

    const getRowsOrder = () => {
        return orderList.reduce((res: any, item, index) => {
            res.push(
                makeStringDeleteRow(
                    item.id,
                    `Коробка #${index + 1} ( ${item.box} , вес брутто 94 г, вес пустой коробки 10 г)`,
                    deleteBoxHandler
                )
            );
            if (item.products.length === 0) {
                res.push(makeStringRow(nanoid(5), `Коробка пуста`));
            }
            return [...res, ...item.products];
        }, []);
    };

    const [tableOrderListData, setTableOrderListData] = useState(getRowsOrder());
    const tableHistoryData = useMemo(() => makeShipmentHistory(10), []);
    const tableOrderListСolumns = useMemo(() => orderListСolumns, []);
    const tableHistoryСolumns = useMemo(() => historyСolumns, []);

    useEffect(() => {
        setTableOrderListData(getRowsOrder());
    }, [orderList]);

    return (
        <PageWrapper h1="">
            <main>
                <Layout cols={6} css={{ marginBottom: scale(3) }}>
                    <Layout.Item col={{ xxxl: 2, md: 6 }}>
                        <Block>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0, paddingBottom: scale(1) }}>
                                    Заказ&nbsp;#{id}&nbsp;от&nbsp;{format(new Date(created), 'dd.MM.yyyy')}
                                </h1>
                            </Block.Header>
                            <Block.Body>
                                <div css={dtStyles}>
                                    <Badge text={statusText} />
                                </div>
                                <div
                                    css={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        padding: `${scale(1, true)}px 0`,
                                    }}
                                >
                                    <p>Последнее изменение:</p>
                                    <p>{format(new Date(created), 'dd.MM.yyyy')}</p>
                                </div>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 2, md: 6 }}>
                        <Block>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0 }}>
                                    Сумма заказа {formatPrice(price - discount)} руб.
                                </h1>
                                <Tooltip content="С учётом скидки" placement="right" maxWidth={scale(30)}>
                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                        <TipIcon />
                                    </button>
                                </Tooltip>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}>Скидка</dt>
                                    <dd css={{ ...ddStyles, color: '#e85446' }}>-&nbsp;{formatPrice(discount)} руб.</dd>
                                    <dt css={dtStyles}>Сумма без скидки</dt>
                                    <dd css={{ ...ddStyles, color: '#e85446' }}>{formatPrice(price)} руб.</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 2, md: 6 }}>
                        <Block>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0 }}>{quantity} ед. товара</h1>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}></dt>
                                    <dd css={ddStyles}>кол-во коробок:&nbsp;{boxes}&nbsp;шт.</dd>
                                    <dt css={dtStyles}>Вес:</dt>
                                    <dd css={ddStyles}>{weigth} г</dd>
                                    <dt css={dtStyles}>Склад отгрузки:</dt>
                                    <dd css={ddStyles}>{warehouse}</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Состав груза</Tabs.Tab>
                        <Tabs.Tab>История</Tabs.Tab>
                    </Tabs.List>
                    <Block>
                        <Block.Body>
                            <Tabs.Panel>
                                <div
                                    css={{
                                        display: 'grid',
                                        gridGap: scale(2),
                                        gridTemplateColumns: '200px 250px auto',
                                    }}
                                >
                                    <Tooltip
                                        content={
                                            <div
                                                css={{
                                                    display: 'grid',
                                                    gridGap: scale(1, true),
                                                    '& > a': {
                                                        width: '100%',
                                                        padding: `${scale(1, true)}px ${scale(1)}px`,
                                                    },
                                                }}
                                            >
                                                <Link to="#">Опись</Link>
                                            </div>
                                        }
                                        trigger="mouseenter focus click "
                                        placement="bottom"
                                    >
                                        <button
                                            css={{
                                                display: 'inline-flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <ExportIcon />
                                            &nbsp;Скачать документы
                                        </button>
                                    </Tooltip>
                                    <Tooltip
                                        content={
                                            <div
                                                css={{
                                                    display: 'grid',
                                                    gridGap: scale(1, true),
                                                    '& > a': {
                                                        width: '100%',
                                                        padding: `${scale(1, true)}px ${scale(1)}px`,
                                                    },
                                                }}
                                            >
                                                <Link to="#">Акт-претензия</Link>
                                                <Link to="#">Акт приема-передачи</Link>
                                                <Link to="#">Карточка сборки</Link>
                                                <Link to="#">Опись</Link>
                                            </div>
                                        }
                                        trigger="mouseenter focus click "
                                        placement="bottom"
                                    >
                                        <button
                                            css={{
                                                display: 'inline-flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <FileIcon />
                                            &nbsp;Скачать шаблоны документов
                                        </button>
                                    </Tooltip>
                                    <div
                                        css={{
                                            display: 'flex',
                                            justifyContent: 'flex-end',
                                        }}
                                    >
                                        <Button
                                            Icon={ExportIcon}
                                            onClick={() => {
                                                setAddingOrderPopupOpen(true);
                                            }}
                                        >
                                            Карточка сборки
                                        </Button>
                                        <Button
                                            css={{ marginLeft: scale(3) }}
                                            Icon={PlusIcon}
                                            onClick={() => {
                                                setAddingOrderPopupOpen(true);
                                            }}
                                        >
                                            Добавить коробку
                                        </Button>
                                    </div>
                                    <Popup
                                        isOpen={isAddingOrderPopupOpen}
                                        onRequestClose={() => {
                                            setAddingOrderPopupOpen(false);
                                        }}
                                        title="Добавление новой коробки"
                                        popupCss={{ minWidth: scale(60) }}
                                    >
                                        <Form
                                            onSubmit={values => {
                                                addBoxHandler(values);
                                            }}
                                            initialValues={{
                                                type: '',
                                            }}
                                        >
                                            <Form.Field name="type" label="Тип коробки">
                                                <Select items={typeBoxes} />
                                            </Form.Field>

                                            <Button
                                                type="submit"
                                                size="sm"
                                                theme="primary"
                                                css={{ marginTop: scale(2) }}
                                            >
                                                Сохранить
                                            </Button>
                                        </Form>
                                    </Popup>
                                </div>

                                <Table
                                    columns={tableOrderListСolumns}
                                    data={tableOrderListData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                            <Tabs.Panel>
                                <Table
                                    columns={tableHistoryСolumns}
                                    data={tableHistoryData}
                                    needCheckboxesCol={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                        </Block.Body>
                    </Block>
                </Tabs>
            </main>
        </PageWrapper>
    );
};

export default Shipment;
