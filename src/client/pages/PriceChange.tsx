import React, { useState, useMemo } from 'react';
import { Button, scale, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import PageWrapper from '@components/PageWrapper';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import { makePricesChanges } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import { useLocation } from 'react-router-dom';
import Legend from '@standart/Legend';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';

const claimStatuses = [
    { label: 'Не важно', value: 'Не важно' },
    { label: 'Новая', value: 'Новая' },
    { label: 'В обработке', value: 'В обработке' },
    { label: 'В продакшен', value: 'В продакшен' },
    { label: 'Загрузка контента', value: 'Загрузка контента' },
    { label: 'Возврат на склад', value: 'Возврат на склад' },
    { label: 'Завершено', value: 'Завершено' },
    { label: 'Отклонена', value: 'Отклонена' },
];

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Автор',
        accessor: 'author',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Кол-во товаров',
        accessor: 'quantity',
    },
    {
        Header: 'Создана',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
];

const PriceChange = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [moreFilters, setMoreFilters] = useState(false);
    const data = useMemo(() => makePricesChanges(9), []);
    const [, setIds] = useState<number[]>([]);
    const [startCreationDate, setStartCreationDate] = useState<Date | null>(null);
    const [endCreationDate, setEndCreationDate] = useState<Date | null>(null);

    return (
        <PageWrapper h1="Заявки на производство контента">
            <DatepickerStyles />
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        claimID: '',
                        claimStatus: [],
                        claimAuthor: '',
                        claimShootingType: [],
                        startCreationDate: null,
                        endCreationDate: null,
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Body>
                        <Layout cols={8}>
                            <Layout.Item col={4}>
                                <Form.Field name="claimID" label="ID заявки" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="claimStatus" label="Статус заявки">
                                    <MultiSelect options={claimStatuses} isMulti={false} />
                                </Form.Field>
                            </Layout.Item>
                            {moreFilters ? (
                                <>
                                    <Layout.Item col={1}>
                                        <Form.FastField name="startCreationDate">
                                            <Legend label="Дата создания от" />
                                            <Datepicker
                                                selectsStart
                                                selected={startCreationDate}
                                                startDate={startCreationDate}
                                                endDate={endCreationDate}
                                                maxDate={endCreationDate}
                                                onChange={setStartCreationDate}
                                            />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.FastField name="endCreationDate">
                                            <Legend label="Дата создания до" />
                                            <Datepicker
                                                selectsEnd
                                                selected={endCreationDate}
                                                startDate={startCreationDate}
                                                endDate={endCreationDate}
                                                minDate={startCreationDate}
                                                onChange={setEndCreationDate}
                                            />
                                        </Form.FastField>
                                    </Layout.Item>
                                </>
                            ) : null}
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>
                            <Button theme="outline" size="sm" onClick={() => setMoreFilters(!moreFilters)}>
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </Button>
                        </div>
                        <div>
                            <Form.Reset size="sm" theme="secondary" type="button">
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>

            <Block>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={data}
                        editRow={row => console.log('rowdata', row)}
                        onRowSelect={setIds}
                    />
                    <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default PriceChange;
