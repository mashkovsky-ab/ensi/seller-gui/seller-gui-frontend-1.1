import React, { useState, useMemo } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { useLocation, Link } from 'react-router-dom';
import { FormikValues } from 'formik';

import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import DropdownButton from '@components/DropdownButton';

import typography from '@scripts/typography';
import { makeRandomData, getRandomItem } from '@scripts/mock';
import { prepareForSelect } from '@scripts/helpers';
import { useURLHelper } from '@scripts/useURLHelper';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import { useCopyToClipBoard } from '@scripts/useCopyToClipBoard';

import Form from '@standart/Form';
import Select from '@standart/Select';
import Pagination from '@standart/Pagination';
import Popup from '@standart/Popup';
import Tooltip from '@standart/Tooltip';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';
import MultiSelect from '@standart/MultiSelect';
import DatepickerStyles from '@standart/Datepicker/presets';
import Datepicker from '@standart/Datepicker';
import Legend from '@standart/Legend';
import DatepickerRange from '@standart/DatepickerRange';

import PlusIcon from '@svg/plus.svg';
import CopyIcon from '@svg/tokens/small/copy.svg';
import SettingsIcon from '@svg/tokens/small/settings.svg';
import ExportIcon from '@svg/tokens/small/export.svg';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import ClosedCircle from '@svg/tokens/small/closedCircle.svg';
import PersentIcon from '@svg/tokens/small/percent.svg';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: '',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название и артикул',
        accessor: 'title',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Категория и бренд',
        accessor: 'categoryAndBrand',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Количество, шт',
        accessor: 'quantity',
    },
    {
        Header: 'Статус проверки',
        accessor: 'checkStatus',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Статус продажи',
        accessor: 'saleStatus',
    },
    {
        Header: 'Обновлено',
        accessor: 'update',
    },
];

const brands = ['lamoda', 'ашан', 'перекресток'];
const brandsForSelect = prepareForSelect(brands);
const categories = ['Шампунь', 'Профессиональные шампуни'];
const categoriesForSelect = prepareForSelect(categories);
const statuses = ['На согласовании', 'В процессе', 'Предзаказ'];
const statusesForSelect = prepareForSelect(statuses);

const getTableItem = (id: number) => ({
    id: `100${id}`,
    photo: 'https://picsum.photos/300/300',
    title: getRandomItem([
        ['Резинка для волос женская и детская, прочная и долговечная (браслет пружинка), набор из 3 шт.', 25435],
        ['Стайлер для волос IKOO E-styler Pro White Platina', 12344],
        ['Декоративная игрушка', 112233],
        ['Подушка', 11223344],
        ['Карандаш', 12344321],
        ['Бутылка', 43214321],
        ['Мячик', 12344321],
        ['Шарик', 1234],
        ['Ноутбук', 4311234],
    ]),
    categoryAndBrand: [getRandomItem(categories), getRandomItem(brands)],
    quantity: getRandomItem([100, 25, 13, 512, 2000, 2000]),
    checkStatus: getRandomItem(statuses),
    saleStatus: getRandomItem(statuses),
    update: getRandomItem(['12 минут наза', '1 месяц назад', '2 месяц назад']),
});

const Filters = ({
    className,
    initialValues,
    onSubmit,
    onReset,
}: {
    className?: string;
    onSubmit: (vals: FormikValues) => void;
    onReset: (vals: FormikValues) => void;
    initialValues: FormikValues;
}) => {
    const [moreFilters, setMoreFilters] = useState(false);
    const { colors } = useTheme();
    return (
        <Block className={className}>
            <Block.Body>
                <DatepickerStyles />
                <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                    <Block.Body>
                        <Layout cols={24}>
                            <Layout.Item col={8}>
                                <Form.FastField name="title" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={8}>
                                <Form.FastField name="brand" label="Бренд">
                                    <MultiSelect options={brandsForSelect} />
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={8}>
                                <Form.FastField name="categories" label="Категории">
                                    <MultiSelect options={categoriesForSelect} />
                                </Form.FastField>
                            </Layout.Item>
                            {moreFilters ? (
                                <>
                                    <Layout.Item col={5}>
                                        <Form.FastField name="offerId" label="Идентификатор оффера" />
                                    </Layout.Item>
                                    <Layout.Item col={5}>
                                        <Form.FastField name="sellerId" label="Идентификатор товара продавца" />
                                    </Layout.Item>
                                    <Layout.Item col={6}>
                                        <Form.FastField name="vendorCode" label="Артикул" />
                                    </Layout.Item>
                                    <Layout.Item col={8} row={3}>
                                        <Form.FastField name="showSelected" css={{ marginBottom: scale(1) }}>
                                            <Checkbox>Показывать только выбранные товары</Checkbox>
                                        </Form.FastField>
                                        <Form.FastField name="showWithoutCert" css={{ marginBottom: scale(1) }}>
                                            <Checkbox>Только товары без сертификата</Checkbox>
                                        </Form.FastField>
                                        <Form.FastField name="showWithoutPhoto">
                                            <Checkbox>Только товары без фотографий</Checkbox>
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={6}>
                                        <Form.FastField name="saleStatus" label="Статус продажи">
                                            <MultiSelect options={statusesForSelect} />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={10}>
                                        <Form.FastField name="checkStatus" label="Статус проверки">
                                            <MultiSelect options={statusesForSelect} />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={8}>
                                        <Form.FastField name="creationDates" label="Дата создания">
                                            <DatepickerRange />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={8}>
                                        <Form.FastField name="updateDates" label="Дата обновления">
                                            <DatepickerRange />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="widthFrom" label="Ширина, мм" placeholder="От" />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.FastField name="widthTo" placeholder="До" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="heigthFrom" label="Высота, мм" placeholder="От" />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.FastField name="heigthTo" placeholder="До" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="depthFrom" label="Глубина, мм" placeholder="От" />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.FastField name="depthTo" placeholder="До" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="weightFrom" label="Высота, мм" placeholder="От" />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.FastField name="weightTo" placeholder="До" />
                                    </Layout.Item>
                                </>
                            ) : null}
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>
                            Найдено 135 товаров{' '}
                            <button
                                type="button"
                                css={{ color: colors?.primary, marginLeft: scale(2) }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>{' '}
                        </div>
                        <div>
                            <Form.Reset size="sm" theme="secondary" type="button">
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block.Body>
        </Block>
    );
};

const CatalogProducts = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const data = useMemo(() => makeRandomData(10, getTableItem), []);
    const [ids, setIds, selectedData] = useSelectedRowsData<typeof data[0]>(data);
    const copyToClipBoard = useCopyToClipBoard(selectedData);

    const emptyInitialValues = {
        title: '',
        brand: '',
        category: '',
        offerId: '',
        sellerId: '',
        vendorCode: '',
        saleStatus: '',
        checkStatus: '',
        creationDates: [null],
        updateDates: [null],
        widthFrom: '',
        widthTo: '',
        heightFrom: '',
        heightTo: '',
        depthFrom: '',
        depthTo: '',
        weightFrom: '',
        weightTo: '',
        showSelected: false,
        showWithoutCert: false,
        showWithoutPhoto: false,
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitialValues);

    return (
        <PageWrapper h1="Товары">
            <>
                <Filters
                    onSubmit={vals => {
                        URLHelper(vals);
                    }}
                    onReset={vals => console.log(vals)}
                    initialValues={initialValues}
                    css={{ marginBottom: scale(3) }}
                />

                <Block>
                    <Block.Header>
                        <div css={{ display: 'flex', alignItems: 'center' }}>
                            <Button theme="primary" size="sm" to="/catalog/products/create" Icon={PlusIcon} as={Link}>
                                Создать товар
                            </Button>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginLeft: scale(2) }}
                                onClick={() => alert('Тут должен быть экспорт')}
                                Icon={ExportIcon}
                            >
                                Экспорт отфильтрованных
                            </Button>
                            <span css={{ marginLeft: scale(2) }}>Выбрано товаров {ids.length}</span>
                            {ids.length > 0 ? (
                                <>
                                    <DropdownButton buttonContent={'Мои документы'} css={{ marginLeft: scale(2) }}>
                                        <DropdownButton.Option onClick={() => console.log('меня нажали')}>
                                            Создать заявку на съемку
                                        </DropdownButton.Option>
                                        <DropdownButton.Option>
                                            <PersentIcon /> Создать заявку на скидку
                                        </DropdownButton.Option>
                                        <DropdownButton.Option onClick={() => console.log('ай')}>
                                            <ClosedCircle /> Снять с продажи
                                        </DropdownButton.Option>
                                        <DropdownButton.Option onClick={copyToClipBoard}>
                                            <CopyIcon /> Копировать ID
                                        </DropdownButton.Option>
                                    </DropdownButton>
                                    <Button
                                        theme="primary"
                                        size="sm"
                                        css={{ marginLeft: scale(2) }}
                                        onClick={() => alert('Тут должен быть экспорт')}
                                        Icon={ExportIcon}
                                    >
                                        Экспорт выбранных
                                    </Button>
                                </>
                            ) : null}
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} needSettingsColumn={false} onRowSelect={setIds} />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
            </>
        </PageWrapper>
    );
};

export default CatalogProducts;
