import React from 'react';
import { scale, Layout, Button } from '@greensight/gds';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import Form from '@standart/Form';
import Select from '@standart/Select';
import DatepickerRange from '@standart/DatepickerRange';
import DatepickerStyles from '@standart/Datepicker/presets';

import { prepareForSelect } from '@scripts/helpers';

const MarketingBundlesCreate = () => {
    return (
        <PageWrapper h1="Создание заявки на бандл">
            <Block css={{ maxWidth: scale(128) }}>
                <Block.Body>
                    <Form
                        initialValues={{ name: '', offers: '', type: '', roubles: '', dates: [null], status: '' }}
                        onSubmit={vals => console.log(vals)}
                    >
                        <DatepickerStyles />
                        <Layout cols={2} css={{ marginBottom: scale(2) }}>
                            <Layout.Item col={2}>
                                <Form.FastField name="name" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.FastField name="offers" label="Офферы" hint="ID офферов через запятую" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.FastField name="type" label="Тип значения">
                                    <Select items={prepareForSelect(['Проценты', 'Рубли'])} />
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.FastField name="roubles" label="Значение в рублях" type="number" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.FastField name="dates" label="Период действия">
                                    <DatepickerRange />
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.FastField name="status" label="Статус">
                                    <Select items={prepareForSelect(['Создана', 'На согласовании'])} />
                                </Form.FastField>
                            </Layout.Item>
                        </Layout>
                        <Button size="sm" type="submit">
                            Сохранить
                        </Button>
                    </Form>
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default MarketingBundlesCreate;
