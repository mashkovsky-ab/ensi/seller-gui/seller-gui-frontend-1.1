import React, { useMemo } from 'react';
import { Helmet } from 'react-helmet-async';
import { scale, useTheme } from '@greensight/gds';
import { format } from 'date-fns';
import { CSSObject } from '@emotion/core';
import { makeRequesters, makeProducts } from '@scripts/mock';
import typography from '@scripts/typography';
import Badge from '@components/Badge';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Tabs from '@standart/Tabs';
import Table from '@components/Table';

const requestItem = makeRequesters(1)[0];
const products = makeProducts(4);

const columns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'name',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Артикул',
        accessor: 'code',
    },
    {
        Header: 'Статус согласования',
        accessor: 'agreed',
    },
    {
        Header: 'Комментарий по статусу согласования',
        accessor: 'comment',
    },
];

const RequestCheckItem = () => {
    const { id, author, created, quantity, status } = requestItem;
    const { colors } = useTheme();

    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: '150px 1fr', margin: `${scale(2)}px 0` };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };

    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    const tableProducts = products.map(p => ({
        id: p.id,
        name: [p.title[0], `/products/catalog/${p.title[1]}`],
        code: p.title[1],
        agreed: p.agreed,
        comment: p.content,
    }));

    const tableData = useMemo(() => tableProducts, []);
    const tableColumns = useMemo(() => columns, []);

    return (
        <PageWrapper h1={`Заявка #${id} от ${format(new Date(created), 'dd.MM.yyyy')}`}>
            <main>
                <Helmet>
                    <title>Заявка #{id}</title>
                </Helmet>
                <div css={{ display: 'inline-block', marginBottom: scale(2) }}>
                    <Block>
                        <Block.Body>
                            <Badge text={status} css={{ marginBottom: scale(2) }} />
                            <dl css={{ ...dlStyles }}>
                                <dt css={dtStyles}>Автор</dt>
                                <dd css={ddStyles}>{author}</dd>
                                <dt css={dtStyles}>Кол-во товара</dt>
                                <dd css={ddStyles}>{quantity}</dd>
                            </dl>
                        </Block.Body>
                    </Block>
                </div>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Товары</Tabs.Tab>
                    </Tabs.List>
                    <Block>
                        <Block.Body>
                            <Tabs.Panel>
                                <Table
                                    columns={tableColumns}
                                    data={tableData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                        </Block.Body>
                    </Block>
                </Tabs>
            </main>
        </PageWrapper>
    );
};

export default RequestCheckItem;
