import React, { useState, useMemo } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { CSSObject } from '@emotion/core';

import typography from '@scripts/typography';
import { STATUSES } from '@scripts/enums';
import { makeRandomData, makeShipmentHistory } from '@scripts/mock';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Badge from '@components/Badge';
import DropdownButton from '@components/DropdownButton';
import Table from '@components/Table';

import Tooltip from '@standart/Tooltip';
import Tabs from '@standart/Tabs';
import Popup from '@standart/Popup';
import Checkbox from '@standart/Checkbox';
import Form from '@standart/Form';
import CheckboxGroup from '@standart/CheckboxGroup';

import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import PlusIcon from '@svg/plus.svg';

const storageColumns = [
    {
        Header: '№ заказа',
        accessor: 'orderNumber',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Дата заказа',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Сумма',
        accessor: 'amount',
    },
    {
        Header: 'Требуемая дата отгрузки',
        accessor: 'shipmentDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Кол-во коробок',
        accessor: 'quantity',
    },
];
const historyСolumns = [
    {
        Header: 'Дата',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
    },
    {
        Header: 'Сущность',
        accessor: 'essense',
    },
    {
        Header: 'Действие',
        accessor: 'action',
    },
];

const getStorageTableItem = (id: number) => {
    return {
        id,
        orderNumber: [`20002-${id}`, `/shipment/list/20002-${id}`],
        date: new Date(),
        amount: 300,
        shipmentDate: new Date(),
        quantity: id + 1,
    };
};

const CargosItem = () => {
    const { id } = useParams<{ id: string }>();
    const [isOpen, setIsOpen] = useState(false);
    const dlStyles: CSSObject = {
        display: 'grid',
        gridTemplate: 'auto/1fr 1fr',
        gap: scale(1),
    };

    const tableStorageColumns = useMemo(() => storageColumns, []);
    const tableStorageData = useMemo(() => makeRandomData(4, getStorageTableItem), []);
    const tableHistoryСolumns = useMemo(() => historyСolumns, []);
    const tableHistoryData = useMemo(() => makeShipmentHistory(10), []);

    return (
        <PageWrapper h1={`Груз ${id}`}>
            <Layout cols={3} css={{ marginBottom: scale(2) }}>
                <Layout.Item>
                    <Block>
                        <Block.Header>
                            <h2 css={typography('h2')}>
                                Груз #{id} от {new Date().toLocaleDateString('ru-RU')}
                            </h2>
                            <Badge text="Создан" type={STATUSES.SUCCESS} />
                        </Block.Header>
                        <Block.Body>
                            <dl css={dlStyles}>
                                <dt>Служба доставки</dt>
                                <dd>B2Cpl</dd>
                                <dt>Последнее изменение:</dt>
                                <dd>{new Date().toLocaleDateString('ru-RU')}</dd>
                            </dl>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item>
                    <Block>
                        <Block.Header>
                            <h2 css={typography('h2')}>
                                Сумма груза 33 руб.{' '}
                                <Tooltip content="С учетом скидки">
                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                        <TipIcon />
                                    </button>
                                </Tooltip>
                            </h2>
                        </Block.Header>
                        <Block.Body>
                            <dl css={dlStyles}>
                                <dt>Скидка</dt>
                                <dd>-427 руб</dd>
                                <dt>Сумма без скидки</dt>
                                <dd>460 руб</dd>
                            </dl>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item>
                    <Block>
                        <Block.Header>
                            <h2 css={typography('h2')}>5 единиц товара</h2>
                        </Block.Header>
                        <Block.Body>
                            <dl css={dlStyles}>
                                <dt>Кол-во коробок</dt>
                                <dd>2шт</dd>
                                <dt>Вес</dt>
                                <dd>1970гр</dd>
                                <dt>Склад отгрузки</dt>
                                <dd>Алабушево</dd>
                            </dl>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
            <Tabs css={{ maxWidth: scale(128) }}>
                <Tabs.List>
                    <Tabs.Tab>Состав груза</Tabs.Tab>
                    <Tabs.Tab>История</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel>
                    <Block>
                        <Block>
                            <Block.Header>
                                <div css={{ display: 'flex' }}>
                                    <DropdownButton buttonContent={'Скачать документы'} css={{ marginRight: scale(1) }}>
                                        <DropdownButton.Option onClick={() => console.log('ай')}>
                                            Акт приёма-передачи
                                        </DropdownButton.Option>
                                    </DropdownButton>
                                    <DropdownButton buttonContent={'Скачать шаблон документов'}>
                                        <DropdownButton.Option onClick={() => console.log('ай')}>
                                            Акт приёма-передачи
                                        </DropdownButton.Option>
                                    </DropdownButton>
                                </div>
                                <Button size="sm" Icon={PlusIcon} onClick={() => setIsOpen(true)}>
                                    Добавить заказы
                                </Button>
                            </Block.Header>
                            <Block.Body>
                                <Table
                                    columns={tableStorageColumns}
                                    data={tableStorageData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Block.Body>
                        </Block>
                    </Block>
                </Tabs.Panel>
                <Tabs.Panel>
                    <Block>
                        <Block>
                            <Block.Body>
                                <Table
                                    columns={tableHistoryСolumns}
                                    data={tableHistoryData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Block.Body>
                        </Block>
                    </Block>
                </Tabs.Panel>
            </Tabs>
            <Popup
                title="Настройка заказов в груз"
                isOpen={isOpen}
                onRequestClose={() => setIsOpen(false)}
                popupCss={{ minWidth: scale(50) }}
            >
                <Form initialValues={{ orders: [] }} onSubmit={val => console.log(val)}>
                    <Form.Field name="orders" label="Выберите заказы" css={{ marginBottom: scale(2) }}>
                        <CheckboxGroup>
                            <Checkbox value="034">1000 2003 03 4</Checkbox>
                            <Checkbox value="0312">1000 2003 03 12</Checkbox>
                        </CheckboxGroup>
                    </Form.Field>
                    <Button size="sm" type="submit">
                        Сохранить
                    </Button>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default CargosItem;
