import React, { useState } from 'react';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import typography from '@scripts/typography';
import Block from '@components/Block';
import Form from '@standart/Form';
import Tabs from '@standart/Tabs';
import Datepicker from '@standart/Datepicker';
import Select, { SelectItemProps } from '@standart/Select';
import Switcher from '@standart/Switcher';
import CheckboxGroup from '@standart/CheckboxGroup';
import * as Yup from 'yup';
import { DELIVERY_SERVICE_STATUSES } from '@scripts/data/different';

const statuses = DELIVERY_SERVICE_STATUSES.map(i => ({ label: i, value: i }));

const DeliveryServiceDetail = () => {
    const { colors } = useTheme();
    const [selectedStatus, setSelectedStatus] = useState<SelectItemProps>(statuses[0]);

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <Layout cols={2} css={{ marginBottom: scale(3) }}>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Body>
                            <Form
                                initialValues={{
                                    name: 'СДЭК',
                                    datepicker: null,
                                    status: '',
                                    priority: '1',
                                }}
                                validationSchema={Yup.object().shape({
                                    name: Yup.string().required('Текст что-то забыли'),
                                    datepicker: Yup.date().nullable().required('А как же дата?'),
                                })}
                                onSubmit={values => console.log(values)}
                            >
                                <div
                                    css={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginBottom: scale(3),
                                    }}
                                >
                                    <h3 css={{ ...typography('h3'), marginTop: 0 }}>Инфопанель</h3>
                                    <div>
                                        <Button size="sm" theme="primary" type="submit">
                                            Сохранить
                                        </Button>
                                        <Form.Reset
                                            size="sm"
                                            theme="secondary"
                                            type="button"
                                            css={{ marginLeft: scale(1) }}
                                        >
                                            Отмена
                                        </Form.Reset>
                                    </div>
                                </div>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="name" label="Название" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="datepicker" label="Дата регистрации">
                                            пофиксить дейтпикер
                                            {/* <Datepicker /> */}
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="status">
                                            <Select
                                                name="status"
                                                label="Статус"
                                                items={statuses}
                                                selectedItem={selectedStatus}
                                                onChange={val => {
                                                    if (val.selectedItem) setSelectedStatus(val.selectedItem);
                                                }}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="priority" label="Приоритет" />
                                    </Layout.Item>
                                </Layout>
                            </Form>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Body>
                            <h3 css={{ ...typography('h3'), marginBottom: scale(2), marginTop: 0 }}>KPIs</h3>
                            <Layout cols={3}>
                                <Layout.Item col={2}>
                                    <h4 css={{ ...typography('bodyMdBold') }}>Количество отправлений</h4>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ textAlign: 'right', ...typography('bodyMd') }}>
                                    210
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <h4 css={{ ...typography('bodyMdBold') }}>Сумма отправлений</h4>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ textAlign: 'right', ...typography('bodyMd') }}>
                                    27 270.00
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
            <Block>
                <Block.Body>
                    <Tabs defaultIndex={0}>
                        <Tabs.List>
                            <Tabs.Tab>Настройки</Tabs.Tab>
                            <Tabs.Tab>Ограничения</Tabs.Tab>
                        </Tabs.List>
                        <Tabs.Panel>
                            <Form
                                initialValues={{ options: [], time: '', services: [] }}
                                onSubmit={values => console.log(values)}
                            >
                                <div css={{ marginBottom: scale(3), textAlign: 'right' }}>
                                    <Button size="sm" theme="primary" type="submit">
                                        Сохранить
                                    </Button>
                                    <Form.Reset
                                        size="sm"
                                        theme="secondary"
                                        type="button"
                                        css={{ marginLeft: scale(1) }}
                                    >
                                        Отмена
                                    </Form.Reset>
                                </div>
                                <Layout cols={2} css={{ marginBottom: scale(4) }}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="options">
                                            <CheckboxGroup>
                                                <Switcher value="consolidate">
                                                    Консолидация многоместных отправлений
                                                </Switcher>
                                                <Switcher value="deconsolidate">Расконсолидация</Switcher>
                                                <Switcher value="mile">Нулевая миля</Switcher>
                                                <Switcher value="express">Экспресс-доставка</Switcher>
                                                <Switcher value="return">Принимает возвраты</Switcher>
                                            </CheckboxGroup>
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="time">
                                            <Select
                                                label="Крайнее время для заданий на забор"
                                                name="time"
                                                value="11:00"
                                                items={[{ value: '11:00', label: '11:00' }]}
                                                selectedItem={{ value: '11:00', label: '11:00' }}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="services">
                                            <CheckboxGroup>
                                                <h4 css={{ ...typography('bodyMdBold') }}>
                                                    Услуги логистического оператора
                                                </h4>
                                                <Switcher value="refuse">Частичный отказ</Switcher>
                                                <Switcher value="returnPosibility">Возможность возврата</Switcher>
                                                <Switcher value="fitting">Примерка</Switcher>
                                                <Switcher value="opening">Вскрытие разрешено</Switcher>
                                                <Switcher value="insurance">Вскрытие разрешено</Switcher>
                                            </CheckboxGroup>
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Form>
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Form initialValues={{ options: [], count: 1 }} onSubmit={values => console.log(values)}>
                                <div css={{ marginBottom: scale(3), textAlign: 'right' }}>
                                    <Button size="sm" theme="primary" type="submit">
                                        Сохранить
                                    </Button>
                                    <Form.Reset
                                        size="sm"
                                        theme="secondary"
                                        type="button"
                                        css={{ marginLeft: scale(1) }}
                                    >
                                        Отмена
                                    </Form.Reset>
                                </div>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="options">
                                            <Switcher value="dangerCargo">Доставка опасных грузов</Switcher>
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field
                                            name="count"
                                            label="Максимальное кол-во отправленией в день"
                                            type="number"
                                        />
                                    </Layout.Item>
                                </Layout>
                            </Form>
                        </Tabs.Panel>
                    </Tabs>
                </Block.Body>
            </Block>
        </main>
    );
};

export default DeliveryServiceDetail;
