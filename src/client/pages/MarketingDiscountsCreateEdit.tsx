import React, { Fragment, useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import * as Yup from 'yup';
import { scale, Layout, Button } from '@greensight/gds';
import Block from '@components/Block';
import Form from '@standart/Form';
import Select, { SelectItemProps } from '@standart/Select';
import MultiSelect from '@standart/MultiSelect';
import DatepickerRange from '@standart/DatepickerRange';
import DatepickerStyles from '@standart/Datepicker/presets';
import Switcher from '@standart/Switcher';
import Table, { TableRowProps } from '@components/Table';
import Popup from '@standart/Popup';
import PageWrapper from '@components/PageWrapper';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import { prepareForSelect } from '@scripts/helpers';
import { MARKETING_DISCOUNTS, MARKETING_DISCOUNTS_STATUSES } from '@scripts/data/different';

const discounts = prepareForSelect(MARKETING_DISCOUNTS);
const saleTypes = prepareForSelect(['Проценты', 'Рубли']);
const statuses = prepareForSelect(MARKETING_DISCOUNTS_STATUSES.slice(0, 2));
const saleConditions = prepareForSelect([
    'Первый заказ',
    'Заказ от определённой суммы',
    'Заказ от определённой суммы заданного бренда',
    'Заказ от определённой суммы заданной категории',
    'Количество единиц одного товара',
    'Способ доставки',
    'Способ оплаты',
    'Регион',
    'Пользователь',
    'Порядковый номер заказа',
    'Суммируется с другими маркетинговыми инструментами',
]);
const brands = prepareForSelect(['Vikki&Lilli', 'Ikoo', 'Rowenta', "L'oreal"]);
const categories = prepareForSelect([
    'Резинки для волос',
    'Стайлеры',
    'Ножницы',
    'Шампуни',
    'Мебель',
    'Шампуни >> Профессиональные шампуни',
    'Шампуни >> Восстанавливающие шампуни',
    'Тест',
    'Шампуни >> Профессиональные шампуни >> Шампуни без парабенов',
    'Мебель >> Тестовая категория',
    'Товары для дома',
    'Товары для дома >> Текстиль для дома',
    'Товары для кухни',
]);
const receivingMethods = prepareForSelect(['Курьерская доставка', 'Самовывоз']);
const paymentMethods = prepareForSelect(['Онлайн']);
const regions = [
    {
        label: 'Центральный федеральный округ',
        options: prepareForSelect([
            'Город федерального значения Москва',
            'Белгородская область',
            'Брянская область',
            'Владимирская область',
            'Воронежская область',
            'Ивановская область',
            'Калужская область',
            'Костромская область',
            'Курская область',
            'Липецкая область',
            'Московская область',
            'Орловская область',
            'Рязанская область',
            'Тамбовская область',
            'Тверская область',
            'Ярославская область',
            'Смоленская область',
            'Тульская область',
        ]),
    },
    {
        label: 'Приволжский федеральный округ',
        options: prepareForSelect([
            'Республика Башкортостан',
            'Республика Марий Эл',
            'Республика Мордовия',
            'Республика Татарстан',
            'Удмуртская Республика',
            'Чувашская Республика',
            'Пермский край',
            'Нижегородская область',
            'Оренбургская область',
            'Пензенская область',
            'Самарская область',
            'Саратовская область',
            'Ульяновская область',
            'Кировская область',
        ]),
    },
    {
        label: 'Северо-Западный федеральный округ',
        options: prepareForSelect([
            'Город федерального значения Санкт-Петербург',
            'Республика Карелия',
            'Республика Коми',
            'Архангельская область',
            'Вологодская область',
            'Калининградская область',
            'Ленинградская область',
            'Мурманская область',
            'Новгородская область',
            'Псковская область',
            'Ненецкий автономный округ',
        ]),
    },
    {
        label: 'Уральский федеральный округ',
        options: prepareForSelect([
            'Курганская область',
            'Свердловская область',
            'Тюменская область',
            'Челябинская область',
            'Ханты-Мансийский автономный округ — Югра',
            'Ямало-Ненецкий автономный округ',
        ]),
    },
    {
        label: 'Южный федеральный округ',
        options: prepareForSelect([
            'Город федерального значения Севастополь',
            'Республика Адыгея',
            'Республика Калмыкия',
            'Республика Крым',
            'Краснодарский край',
            'Астраханская область',
            'Волгоградская область',
            'Ростовская область',
        ]),
    },
    {
        label: 'Сибирский федеральный округ',
        options: prepareForSelect([
            'Республика Алтай',
            'Республика Тыва',
            'Республика Хакасия',
            'Алтайский край',
            'Красноярский край',
            'Иркутская область',
            'Кемеровская область',
            'Новосибирская область',
            'Омская область',
            'Томская область',
        ]),
    },
    {
        label: 'Северо-Кавказский федеральный округ',
        options: prepareForSelect([
            'Республика Дагестан',
            'Республика Ингушетия',
            'Кабардино-Балкарская Республика',
            'Карачаево-Черкесская Республика',
            'Республика Северная Осетия',
            'Чеченская Республика',
            'Ставропольский край',
        ]),
    },
    {
        label: 'Дальневосточный федеральный округ',
        options: prepareForSelect([
            'Республика Бурятия',
            'Республика Саха (Якутия)',
            'Забайкальский край',
            'Камчатский край',
            'Приморский край',
            'Хабаровский край',
            'Амурская область',
            'Магаданская область',
            'Сахалинская область',
            'Еврейская автономная область',
            'Чукотский автономный округ',
        ]),
    },
];
const segments = prepareForSelect(['A', 'B', 'C']);
const roles = prepareForSelect(['Профессионал', 'Реферальный партнёр']);
const otherSales = prepareForSelect([
    'Заявка на бандл (с 07.05.2021 по 13.05.2021)',
    'Пример скидки (с 07.05.2021 по 31.08.2021)',
    'Скидка на расческу (Бессрочная)',
    'Новый бандл (с 14.04.2021 по 16.04.2021)',
    'Бандл ! (с 26.03.2021)',
    '11111111 (Бессрочная)',
    '1212 (Бессрочная)',
    'Скидка (с 19.01.2021 по 24.01.2021)',
    'Скидка за комплект - 500 рублей (с 28.12.2020 по 10.01.2021)',
    '1+1=3 (с 15.10.2020 по 18.10.2020)',
    'Скидка 1+1=3 (с 15.10.2020 по 18.10.2020)',
    'Скидка на стайлеры (с 19.10.2020 по 25.10.2020)',
    'Резинка и стайлер (с 14.10.2020 по 18.10.2020)',
    'Скидка на стайлер по промокоду 10% (с 14.10.2020 по 18.10.2020)',
]);

interface IDiscountConditionProps {
    saleSum?: number;
    saleBrands?: Record<string, string>[];
    saleCategory?: Record<string, string>[];
    saleAmount?: number;
    saleOffer?: number;
    saleReceivingMethod?: Record<string, string>[];
    salePaymentMethod?: Record<string, string>[];
    saleRegions?: Record<string, string>[];
    saleSegments?: Record<string, string>[];
    saleRoles?: Record<string, string>[];
    saleOrderNum?: number;
    saleOtherSales?: Record<string, string>[];
}

const FillUpTableForm = ({
    saleConditionsI,
    setSaleConditionsI,
    addDiscountCondition,
}: {
    saleConditionsI: SelectItemProps[];
    setSaleConditionsI: (saleConditionsI: SelectItemProps[]) => void;
    addDiscountCondition: (condition: string, rowData: IDiscountConditionProps) => void;
}) => {
    const [saleCondition, setSaleCondition] = useState<SelectItemProps | null>(null);

    return (
        <Form
            initialValues={{
                saleCondition: null,
                saleSum: '',
                saleBrands: null,
                saleCategory: null,
                saleAmount: '',
                saleOffer: '',
                saleReceivingMethod: null,
                salePaymentMethod: null,
                saleRegions: null,
                saleSegments: null,
                saleRoles: null,
                saleOrderNum: '',
                saleOtherSales: null,
            }}
            validationSchema={Yup.object().shape({
                saleCondition: Yup.string().required('Выберите хотя бы одну категорию!').nullable(),
                saleSum: Yup.number().when('saleCondition', {
                    is: value =>
                        [saleConditions[1], saleConditions[2], saleConditions[3]].map(con => con.value).includes(value),
                    then: Yup.number().required('Обязательное поле!'),
                    otherwise: Yup.number(),
                }),
                saleBrands: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[2].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                saleCategory: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[3].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                saleAmount: Yup.number().when('saleCondition', {
                    is: value => value === saleConditions[4].value,
                    then: Yup.number().required('Обязательное поле!'),
                    otherwise: Yup.number(),
                }),
                saleOffer: Yup.number().when('saleCondition', {
                    is: value => value === saleConditions[4].value,
                    then: Yup.number().required('Обязательное поле!'),
                    otherwise: Yup.number(),
                }),
                saleReceivingMethod: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[5].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                salePaymentMethod: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[6].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                saleRegions: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[7].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                saleSegments: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[8].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                saleRoles: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[8].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
                saleOrderNum: Yup.number().when('saleCondition', {
                    is: value => value === saleConditions[9].value,
                    then: Yup.number().required('Обязательное поле!'),
                    otherwise: Yup.number(),
                }),
                saleOtherSales: Yup.array().when('saleCondition', {
                    is: value => value === saleConditions[10].value,
                    then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                    otherwise: Yup.array().nullable(),
                }),
            })}
            onSubmit={(values, { resetForm }) => {
                const newSaleConditionsI = saleConditionsI.filter(({ value }) => value !== saleCondition?.value);
                setSaleConditionsI(newSaleConditionsI);
                addDiscountCondition(
                    values.saleCondition,
                    Object.fromEntries(
                        Object.entries(values).filter(([k, v]) => v !== null && v !== '' && k !== 'saleCondition')
                    )
                );
                setSaleCondition(null);
                resetForm();
            }}
        >
            <Layout cols={8} align={'end'}>
                <Layout.Item col={6}>
                    <Form.Field name="saleCondition" label="Условия предоставления скидки">
                        <Select
                            items={saleConditionsI}
                            placeholder=""
                            selectedItem={saleCondition}
                            onChange={({ selectedItem }) => {
                                if (selectedItem) setSaleCondition(selectedItem);
                            }}
                        />
                    </Form.Field>
                </Layout.Item>
                <Layout.Item col={2}>
                    <Button type="submit" size="sm">
                        Добавить условие
                    </Button>
                </Layout.Item>
                {(saleCondition?.value === saleConditions[1].value ||
                    saleCondition?.value === saleConditions[2].value ||
                    saleCondition?.value === saleConditions[3].value) && (
                    <>
                        <Layout.Item col={4}>
                            <Form.Field name="saleSum" type="number" label="От (руб.)" min={0} />
                        </Layout.Item>
                        <Layout.Item col={4}></Layout.Item>
                    </>
                )}
                {saleCondition?.value === saleConditions[2].value && (
                    <Layout.Item col={8}>
                        <Form.FastField name="saleBrands" label="Бренды">
                            <MultiSelect options={brands} />
                        </Form.FastField>
                    </Layout.Item>
                )}
                {saleCondition?.value === saleConditions[3].value && (
                    <Layout.Item col={8}>
                        <Form.FastField name="saleCategory" label="Категории">
                            <MultiSelect options={categories} />
                        </Form.FastField>
                    </Layout.Item>
                )}
                {saleCondition?.value === saleConditions[4].value && (
                    <>
                        <Layout.Item col={4}>
                            <Form.Field name="saleAmount" type="number" label="Количество" min={0} />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.Field name="saleOffer" type="number" label="Оффер" min={0} />
                        </Layout.Item>
                    </>
                )}
                {saleCondition?.value === saleConditions[5].value && (
                    <>
                        <Layout.Item col={4}>
                            <Form.FastField name="saleReceivingMethod" label="Способ доставки">
                                <MultiSelect options={receivingMethods} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={4}></Layout.Item>
                    </>
                )}
                {saleCondition?.value === saleConditions[6].value && (
                    <>
                        <Layout.Item col={4}>
                            <Form.FastField name="salePaymentMethod" label="Способ оплаты">
                                <MultiSelect options={paymentMethods} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={4}></Layout.Item>
                    </>
                )}
                {saleCondition?.value === saleConditions[7].value && (
                    <Layout.Item col={8}>
                        <Form.FastField name="saleRegions" label="Регионы">
                            <MultiSelect options={regions} />
                        </Form.FastField>
                    </Layout.Item>
                )}
                {saleCondition?.value === saleConditions[8].value && (
                    <>
                        <Layout.Item col={4}>
                            <Form.FastField name="saleSegments" label="Сегменты">
                                <MultiSelect options={segments} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.FastField name="saleRoles" label="Роли">
                                <MultiSelect options={roles} />
                            </Form.FastField>
                        </Layout.Item>
                    </>
                )}
                {saleCondition?.value === saleConditions[9].value && (
                    <>
                        <Layout.Item col={4}>
                            <Form.Field name="saleOrderNum" type="number" label="Порядковый номер заказа" min={0} />
                        </Layout.Item>
                        <Layout.Item col={4}></Layout.Item>
                    </>
                )}
                {saleCondition?.value === saleConditions[10].value && (
                    <>
                        <Layout.Item col={6}>
                            <Form.FastField name="saleOtherSales" label="Суммируется с другими скидками">
                                <MultiSelect options={otherSales} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={2}></Layout.Item>
                    </>
                )}
            </Layout>
        </Form>
    );
};

const MarketingDiscountsCreateEdit = () => {
    const { id } = useParams<{ id?: string }>();
    const [initialValues, setInitialValues] = useState<Record<string, any>>({
        title: '',
        sale: null,
        offersIds: '',
        brands: null,
        category: null,
        exeptOffers: '',
        exeptBrands: null,
        saleType: null,
        saleAmount: '',
        datepickerRange: null,
        saleStatus: statuses[0].value,
        salePromocode: false,
    });

    const [saleItem, setSaleItem] = useState<string | null>(null);
    const [saleType, setSaleType] = useState<string | null>(null);
    const [saleConditionsI, setSaleConditionsI] = useState<SelectItemProps[]>(saleConditions);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isChangeOpen, setIsChangeOpen] = useState(false);

    const makeTableList = (list: Record<string, string>[]) => (
        <ul css={{ paddingTop: scale(1), marginLeft: scale(4), marginBottom: scale(2) }}>
            {list.map(({ value }, index) => (
                <li key={index} css={{ listStyle: 'disc' }}>{`${value}`}</li>
            ))}
        </ul>
    );

    const [data, setData] = useState<TableRowProps[]>([]);
    const [selectedIds, setSelectedIds, selectedRows] = useSelectedRowsData<TableRowProps>(data);
    const conditionsTableColums = [
        {
            Header: '#',
            accessor: 'id',
        },
        {
            Header: 'Условие',
            accessor: 'condition',
        },
        {
            Header: 'Значение',
            accessor: 'value',
            customRender: true,
            Cell: ({ value, row }: any) => {
                let cellRender = null;
                const { condition } = row.values;

                switch (condition) {
                    case saleConditions[1].value:
                        cellRender = value.saleSum;
                        break;
                    case saleConditions[2].value:
                        cellRender = (
                            <>
                                На бренды{makeTableList(value.saleBrands)}от {value.saleSum} руб.
                            </>
                        );
                        break;
                    case saleConditions[3].value:
                        cellRender = (
                            <>
                                На категории{makeTableList(value.saleCategory)}от {value.saleSum} руб.
                            </>
                        );
                        break;
                    case saleConditions[4].value:
                        cellRender = `Оффер ${value.saleOffer}, количество ${value.saleAmount} шт.`;
                        break;
                    case saleConditions[5].value:
                        cellRender = <>На способы доставки{makeTableList(value.saleReceivingMethod)}</>;
                        break;
                    case saleConditions[6].value:
                        cellRender = <>На способы оплаты{makeTableList(value.salePaymentMethod)}</>;
                        break;
                    case saleConditions[7].value:
                        cellRender = <>На регионы{makeTableList(value.saleRegions)}</>;
                        break;
                    case saleConditions[8].value:
                        cellRender = (
                            <>
                                Сегменты{makeTableList(value.saleSegments)}Роли{makeTableList(value.saleRoles)}
                            </>
                        );
                        break;
                    case saleConditions[9].value:
                        cellRender = `${value.saleOrderNum}-й зайказ Клиента`;
                        break;
                    case saleConditions[10].value:
                        cellRender = <>Суммируется со скидками:{makeTableList(value.saleOtherSales)}</>;
                        break;
                    default:
                }

                return cellRender;
            },
        },
    ];

    const addDiscountCondition = useCallback((condition: string, value: IDiscountConditionProps) => {
        setData(prevData => [
            ...prevData,
            {
                id: prevData.length,
                condition,
                value,
            },
        ]);
    }, []);

    useEffect(() => {
        if (id) {
            const values = {
                title: 'Скидочки 22',
                sale: discounts[0].value,
                offersIds: '12, 13, 32',
                saleType: saleTypes[0].value,
                saleAmount: 30,
                datepickerRange: [new Date('11.06.21'), new Date('11.07.21')],
                salePromocode: true,
            };
            if (values?.sale) setSaleItem(values.sale);
            if (values?.saleType) setSaleType(values.saleType);
            setInitialValues(prevState => ({
                ...prevState,
                ...values,
            }));
            addDiscountCondition(saleConditions[2].value, {
                saleSum: 5000,
                saleBrands: [brands[0], brands[1]],
            });
        }
    }, [id, addDiscountCondition]);

    return (
        <>
            <PageWrapper h1={id ? `Редактирование: #${id} "${initialValues.title}"` : 'Создание заявки на скидку'}>
                <DatepickerStyles />
                <Layout cols={12}>
                    <Layout.Item col={8}>
                        <Block>
                            <Block.Body>
                                <Form
                                    id="create-discount"
                                    enableReinitialize={true}
                                    initialValues={initialValues}
                                    validationSchema={Yup.object().shape({
                                        title: Yup.string().required('Обязательное поле!'),
                                        sale: Yup.string().required('Выберите хотя бы одну категорию!').nullable(),
                                        offersIds: Yup.string().when('sale', {
                                            is: value =>
                                                [discounts[0], discounts[12]].map(d => d.value).includes(value),
                                            then: Yup.string().required('Обязательное поле!'),
                                            otherwise: Yup.string(),
                                        }),
                                        brands: Yup.array().when('sale', {
                                            is: value => value === discounts[1].value,
                                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                                            otherwise: Yup.array().nullable(),
                                        }),
                                        category: Yup.array().when('sale', {
                                            is: value => value === discounts[2].value,
                                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                                            otherwise: Yup.array().nullable(),
                                        }),
                                        exeptOffers: Yup.string().when('sale', {
                                            is: value => [discounts[1], discounts[2]].map(d => d.value).includes(value),
                                            then: Yup.string().required('Обязательное поле!'),
                                            otherwise: Yup.string(),
                                        }),
                                        exeptBrands: Yup.array().when('sale', {
                                            is: value => value === discounts[2].value,
                                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                                            otherwise: Yup.array().nullable(),
                                        }),
                                        saleType: Yup.string().required('Выберите хотя бы одну категорию!').nullable(),
                                        saleAmount: Yup.number().required('Обязательное поле!'),
                                        datepickerRange: Yup.array().required('Обязательное поле!').nullable(),
                                        saleStatus: Yup.string().required('Обязательное поле!'),
                                        salePromocode: Yup.boolean().required('Обязательное поле!'),
                                    })}
                                    onSubmit={() => {
                                        console.log('submit');
                                    }}
                                >
                                    <Layout cols={8} align={'end'} css={{ marginBottom: scale(3) }}>
                                        <Layout.Item col={8}>
                                            <Form.Field name="title" label="Название" />
                                        </Layout.Item>
                                        <Layout.Item col={3} align={'start'}>
                                            <Form.Field name="sale" label="Скидка на">
                                                <Select
                                                    items={discounts}
                                                    placeholder=""
                                                    onChange={({ selectedItem }) => {
                                                        if (selectedItem) setSaleItem(selectedItem.value);
                                                    }}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={5}>
                                            {(saleItem === discounts[0].value || saleItem === discounts[12].value) && (
                                                <Form.Field
                                                    type="string"
                                                    name="offersIds"
                                                    label="Офферы"
                                                    placeholder="ID офферов через запятую"
                                                />
                                            )}
                                            {saleItem === discounts[1].value && (
                                                <Form.FastField name="brands" label="Бренды">
                                                    <MultiSelect options={brands} />
                                                </Form.FastField>
                                            )}
                                            {saleItem === discounts[2].value && (
                                                <Form.FastField name="category" label="Категории">
                                                    <MultiSelect options={categories} />
                                                </Form.FastField>
                                            )}
                                        </Layout.Item>
                                        {saleItem === discounts[1].value && (
                                            <>
                                                <Layout.Item col={3}></Layout.Item>
                                                <Layout.Item col={5}>
                                                    <Form.Field
                                                        type="string"
                                                        name="exeptOffers"
                                                        label="За исключением офферов"
                                                        placeholder="ID офферов через запятую"
                                                    />
                                                </Layout.Item>
                                            </>
                                        )}
                                        {saleItem === discounts[2].value && (
                                            <>
                                                <Layout.Item col={3}></Layout.Item>
                                                <Layout.Item col={5}>
                                                    <Form.FastField name="exeptBrands" label="За исключением брендов">
                                                        <MultiSelect options={brands} />
                                                    </Form.FastField>
                                                </Layout.Item>
                                                <Layout.Item col={3}></Layout.Item>
                                                <Layout.Item col={5}>
                                                    <Form.Field
                                                        type="number"
                                                        name="exeptOffers"
                                                        label="За исключением офферов"
                                                        placeholder="ID офферов через запятую"
                                                    />
                                                </Layout.Item>
                                            </>
                                        )}
                                        <Layout.Item col={3}>
                                            <Layout.Item col={3}>
                                                <Form.Field name="saleType" label="Тип значения">
                                                    <Select
                                                        items={saleTypes}
                                                        onChange={({ selectedItem }) => {
                                                            if (selectedItem) setSaleType(selectedItem.value);
                                                        }}
                                                    />
                                                </Form.Field>
                                            </Layout.Item>
                                        </Layout.Item>
                                        <Layout.Item col={3}>
                                            <Form.Field
                                                type="number"
                                                name="saleAmount"
                                                label={`Значение в ${
                                                    saleType !== saleTypes[0].value ? 'рублях' : 'процентах'
                                                }`}
                                                placeholder={`${
                                                    saleType === saleTypes[0].value ? 'Значение от 1 до 100' : ''
                                                }`}
                                                {...(saleType === saleTypes[0].value ? { min: '1', max: '100' } : {})}
                                            />
                                        </Layout.Item>
                                        <Layout.Item col={6}>
                                            <Form.Field name="datepickerRange" label="Период действия скидки">
                                                <DatepickerRange />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={3}>
                                            <Form.FastField name="saleStatus" label="Статус">
                                                <Select
                                                    items={statuses}
                                                    defaultValue={statuses[0].value}
                                                    placeholder=""
                                                />
                                            </Form.FastField>
                                        </Layout.Item>
                                        <Layout.Item col={5}></Layout.Item>
                                        <Layout.Item col={6}>
                                            <Form.FastField name="salePromocode">
                                                <Switcher>
                                                    <span>Скидка действительна только по промокоду</span>
                                                </Switcher>
                                            </Form.FastField>
                                        </Layout.Item>
                                        {data.length > 0 ? (
                                            <Layout.Item col={8} css={{ marginBottom: scale(2) }}>
                                                <div css={{ display: 'flex', justifyContent: 'space-between' }}>
                                                    <p css={{ marginBottom: scale(2) }}>
                                                        <b>Условия предоставления скидки</b>
                                                    </p>
                                                    {selectedIds.length > 0 && (
                                                        <div>
                                                            {selectedIds.length === 1 && (
                                                                <Button
                                                                    type="button"
                                                                    size="sm"
                                                                    css={{ marginRight: scale(2) }}
                                                                    onClick={() => setIsChangeOpen(true)}
                                                                >
                                                                    Редактировать условиe
                                                                </Button>
                                                            )}
                                                            <Button
                                                                type="button"
                                                                size="sm"
                                                                onClick={() => setIsDeleteOpen(true)}
                                                            >
                                                                {`Удалить услови${selectedIds.length > 1 ? 'я' : 'е'}`}
                                                            </Button>
                                                        </div>
                                                    )}
                                                </div>
                                                <Table
                                                    columns={conditionsTableColums}
                                                    data={data}
                                                    needSettingsColumn={false}
                                                    onRowSelect={setSelectedIds}
                                                />
                                            </Layout.Item>
                                        ) : null}
                                    </Layout>
                                </Form>
                                <FillUpTableForm
                                    saleConditionsI={saleConditionsI}
                                    setSaleConditionsI={setSaleConditionsI}
                                    addDiscountCondition={addDiscountCondition}
                                />
                                <Layout.Item col={2}>
                                    <Button
                                        size="md"
                                        type="submit"
                                        form="create-discount"
                                        css={{ marginTop: scale(5) }}
                                    >
                                        Сохранить
                                    </Button>
                                </Layout.Item>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
            </PageWrapper>

            <Popup
                isOpen={isDeleteOpen}
                onRequestClose={() => setIsDeleteOpen(false)}
                title={`Вы уверены, что хотите удалить следующ${selectedIds.length > 1 ? 'ие условия' : 'ее условие'}?`}
                popupCss={{ minWidth: scale(50) }}
            >
                <ul css={{ marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                            #{r.id} – {r.condition}
                        </li>
                    ))}
                </ul>
                <Button
                    size="sm"
                    onClick={() => {
                        setIsDeleteOpen(false);
                        const updatedData = data
                            .filter(row => !selectedIds.includes(row.id))
                            .map((row, index) => {
                                row.id = index;
                                return row;
                            });
                        setData(updatedData);
                        setSelectedIds([]);
                        setSaleConditionsI(
                            saleConditions.filter(
                                condition => !updatedData.map(row => row.condition).includes(condition.value)
                            )
                        );
                    }}
                >
                    Удалить
                </Button>
            </Popup>

            <Popup
                isOpen={isChangeOpen}
                onRequestClose={() => setIsChangeOpen(false)}
                title="Обновление условия"
                popupCss={{ minWidth: scale(50) }}
            >
                <ul css={{ marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                            #{r.id} – {r.condition}
                        </li>
                    ))}
                </ul>
                <Form
                    initialValues={{
                        saleCondition: selectedRows[0]?.condition || null,
                        saleSum: selectedRows[0]?.value?.saleSum || '',
                        saleBrands: selectedRows[0]?.value?.saleBrands || null,
                        saleCategory: selectedRows[0]?.value?.saleCategory || null,
                        saleAmount: selectedRows[0]?.value?.saleAmount || '',
                        saleOffer: selectedRows[0]?.value?.saleOffer || '',
                        saleReceivingMethod: selectedRows[0]?.value?.saleReceivingMethod || null,
                        salePaymentMethod: selectedRows[0]?.value?.salePaymentMethod || null,
                        saleRegions: selectedRows[0]?.value?.saleRegions || null,
                        saleSegments: selectedRows[0]?.value?.saleSegments || null,
                        saleRoles: selectedRows[0]?.value?.saleRoles || null,
                        saleOrderNum: selectedRows[0]?.value?.saleOrderNum || '',
                        saleOtherSales: selectedRows[0]?.value?.saleOtherSales || null,
                    }}
                    validationSchema={Yup.object().shape({
                        saleSum: Yup.number().when('saleCondition', {
                            is: value =>
                                [saleConditions[1], saleConditions[2], saleConditions[3]]
                                    .map(con => con.value)
                                    .includes(value),
                            then: Yup.number().required('Обязательное поле!'),
                            otherwise: Yup.number(),
                        }),
                        saleBrands: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[2].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        saleCategory: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[3].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        saleAmount: Yup.number().when('saleCondition', {
                            is: value => value === saleConditions[4].value,
                            then: Yup.number().required('Обязательное поле!'),
                            otherwise: Yup.number(),
                        }),
                        saleOffer: Yup.number().when('saleCondition', {
                            is: value => value === saleConditions[4].value,
                            then: Yup.number().required('Обязательное поле!'),
                            otherwise: Yup.number(),
                        }),
                        saleReceivingMethod: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[5].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        salePaymentMethod: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[6].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        saleRegions: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[7].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        saleSegments: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[8].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        saleRoles: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[8].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                        saleOrderNum: Yup.number().when('saleCondition', {
                            is: value => value === saleConditions[9].value,
                            then: Yup.number().required('Обязательное поле!'),
                            otherwise: Yup.number(),
                        }),
                        saleOtherSales: Yup.array().when('saleCondition', {
                            is: value => value === saleConditions[10].value,
                            then: Yup.array().required('Выберите хотя бы одну категорию!').nullable(),
                            otherwise: Yup.array().nullable(),
                        }),
                    })}
                    onSubmit={(values, { resetForm }) => {
                        setIsChangeOpen(false);
                        setData(
                            data.map(row => {
                                if (row.id === selectedIds[0]) {
                                    row.value = Object.fromEntries(
                                        Object.entries(values).filter(([, v]) => v !== null)
                                    );
                                }
                                return row;
                            })
                        );
                        resetForm();
                    }}
                >
                    <Layout cols={8} align={'end'} css={{ marginBottom: scale(3) }}>
                        {selectedRows.map(row => (
                            <Fragment key={row.id}>
                                {(row?.condition === saleConditions[1].value ||
                                    row?.condition === saleConditions[2].value ||
                                    row?.condition === saleConditions[3].value) && (
                                    <Layout.Item col={8}>
                                        <Form.Field name="saleSum" type="number" label="От (руб.)" min={0} />
                                    </Layout.Item>
                                )}
                                {row?.condition === saleConditions[2].value && (
                                    <Layout.Item col={8}>
                                        <Form.FastField name="saleBrands" label="Бренды">
                                            <MultiSelect options={brands} />
                                        </Form.FastField>
                                    </Layout.Item>
                                )}
                                {row?.condition === saleConditions[3].value && (
                                    <Layout.Item col={8}>
                                        <Form.FastField name="saleCategory" label="Категории">
                                            <MultiSelect options={categories} />
                                        </Form.FastField>
                                    </Layout.Item>
                                )}
                                {row?.condition === saleConditions[4].value && (
                                    <>
                                        <Layout.Item col={8}>
                                            <Form.Field name="saleAmount" type="number" label="Количество" min={0} />
                                        </Layout.Item>
                                        <Layout.Item col={8}>
                                            <Form.Field name="saleOffer" type="number" label="Оффер" min={0} />
                                        </Layout.Item>
                                    </>
                                )}
                                {row?.condition === saleConditions[5].value && (
                                    <>
                                        <Layout.Item col={8}>
                                            <Form.FastField name="saleReceivingMethod" label="Способ доставки">
                                                <MultiSelect options={receivingMethods} />
                                            </Form.FastField>
                                        </Layout.Item>
                                    </>
                                )}
                                {row?.condition === saleConditions[6].value && (
                                    <Layout.Item col={8}>
                                        <Form.FastField name="salePaymentMethod" label="Способ оплаты">
                                            <MultiSelect options={paymentMethods} />
                                        </Form.FastField>
                                    </Layout.Item>
                                )}
                                {row?.condition === saleConditions[7].value && (
                                    <Layout.Item col={8}>
                                        <Form.FastField name="saleRegions" label="Регионы">
                                            <MultiSelect options={regions} />
                                        </Form.FastField>
                                    </Layout.Item>
                                )}
                                {row?.condition === saleConditions[8].value && (
                                    <>
                                        <Layout.Item col={8}>
                                            <Form.FastField name="saleSegments" label="Сегменты">
                                                <MultiSelect options={segments} />
                                            </Form.FastField>
                                        </Layout.Item>
                                        <Layout.Item col={8}>
                                            <Form.FastField name="saleRoles" label="Роли">
                                                <MultiSelect options={roles} />
                                            </Form.FastField>
                                        </Layout.Item>
                                    </>
                                )}
                                {row?.condition === saleConditions[9].value && (
                                    <Layout.Item col={8}>
                                        <Form.Field
                                            name="saleOrderNum"
                                            type="number"
                                            label="Порядковый номер заказа"
                                            min={0}
                                        />
                                    </Layout.Item>
                                )}
                                {row?.condition === saleConditions[10].value && (
                                    <Layout.Item col={8}>
                                        <Form.FastField name="saleOtherSales" label="Суммируется с другими скидками">
                                            <MultiSelect options={otherSales} />
                                        </Form.FastField>
                                    </Layout.Item>
                                )}
                            </Fragment>
                        ))}
                    </Layout>
                    <Button size="sm" type="submit">
                        Изменить условие
                    </Button>
                </Form>
            </Popup>
        </>
    );
};

export default MarketingDiscountsCreateEdit;
