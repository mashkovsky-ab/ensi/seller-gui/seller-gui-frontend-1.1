import React from 'react';
import { useParams } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { CSSObject } from '@emotion/core';
import { nanoid } from 'nanoid';

import typography from '@scripts/typography';
import Form from '@standart/Form';
import { FieldArray, useFormikContext } from 'formik';
import Datepicker from '@standart/Datepicker';
import DatepickerRange from '@standart/DatepickerRange';
import DatepickerStyles from '@standart/Datepicker/presets';
import Select from '@standart/Select';
import Block from '@components/Block';
import Legend from '@standart/Legend';
import Checkbox from '@standart/Checkbox';
import Mask from '@standart/Mask';

import { sellers } from '@scripts/mock';
import { DAYS } from '@scripts/constants';
import { maskPhone } from '@scripts/mask';

import TrashIcon from '@svg/tokens/small/trash.svg';
import PlusIcon from '@svg/plus.svg';
import PageWrapper from '@components/PageWrapper';
import { prepareForSelect } from '@scripts/helpers';

const SELLERS = prepareForSelect(sellers);

const columnsschedule = [
    {
        Header: 'ID',
        accessor: 'check',
    },
    {
        Header: 'День недели',
        accessor: 'day',
    },
    {
        Header: 'Время работы',
        accessor: 'time',
    },
];

const columnsShipment = [
    {
        Header: 'День недели',
        accessor: 'day',
        type: 'string',
    },
    {
        Header: 'Все службы доставки',
        accessor: 'all',
        type: 'time',
    },
    {
        Header: 'B2Cpl',
        accessor: 'b2cpl',
        type: 'interval',
    },
    {
        Header: 'Boxberry',
        accessor: 'boxberry',
        type: 'interval',
    },
    {
        Header: 'СДЭК',
        accessor: 'sdek',
        type: 'interval',
    },
];

const columnsContact = [
    {
        Header: 'Контактное лицо',
        accessor: 'contact',
    },
    {
        Header: 'Телефон',
        accessor: 'phone',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
];

export interface Store {
    seller: { label: string; value: string };
    title: string;
    code: string;
    adress: string;
    entrance: string;
    floor: string;
    intercom: string;
    comment: string;
}

const storeScheduleTable = DAYS.map(item => {
    return {
        id: `storeschedule${item}`,
        day: item,
        check: true,
        time: [new Date(2021, 12, 31, 9, 0, 0), new Date(2021, 12, 31, 18, 0, 0)],
    };
});

const storeShipmentTable = DAYS.map(item => {
    return {
        id: nanoid(),
        day: item,
        all: new Date(2021, 12, 31, 6, 30, 0),
        b2cpl: {
            create: new Date(2021, 12, 31, 6, 30, 0),
            shipment: [new Date(2021, 12, 31, 9, 0, 0), new Date(2021, 12, 31, 19, 0, 0)],
        },
        boxberry: {
            create: new Date(2021, 12, 31, 6, 30, 0),
            shipment: [new Date(2021, 12, 31, 9, 0, 0), new Date(2021, 12, 31, 19, 0, 0)],
        },
        sdek: {
            create: new Date(2021, 12, 31, 6, 30, 0),
            shipment: [new Date(2021, 12, 31, 9, 0, 0), new Date(2021, 12, 31, 19, 0, 0)],
        },
    };
});

const storeContactTable = [
    {
        id: nanoid(),
        contact: 'Кладовщик',
        phone: '+71234567890',
        email: 'email@mail.ru',
    },
    {
        id: nanoid(),
        contact: 'Оператор',
        phone: '+70987655432',
        email: 'mail@mail.ru',
    },
];

const store = {
    seller: { label: 'Ашан', value: 'Ашан' },
    title: 'Силино',
    code: '100',
    adress: '124460, г Москва, г Зеленоград, р-н Силино, к 1206А',
    entrance: '',
    floor: '',
    intercom: '',
    comment: 'Главный склад маркетплейса',
};

const TableHead = ({ Header, lineCSS }: { Header: React.ReactNode; lineCSS: CSSObject }) => (
    <th css={{ ...typography('bodySmBold'), ...lineCSS, textAlign: 'left' }}>{Header}</th>
);

const StoreEdit = () => {
    return (
        <Block css={{ maxWidth: scale(128) }}>
            <Block.Body>
                <Layout cols={3}>
                    <Layout.Item col={3}>
                        <Form.FastField name="store.seller">
                            <Select name="store.seller" label="Продавец" items={SELLERS} />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="store.title" label="Название склада" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.FastField name="store.code" label="Внешний код" value="100" />
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="store.adress" label="Адрес" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.FastField name="store.entrance" label="Подъезд" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.FastField name="store.floor" label="Этаж" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Form.FastField name="store.intercom" label="Домофон" />
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="store.comment" label="Комментарий к адресу" />
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};

const Contacts = ({ lineCSS }: { lineCSS: CSSObject }) => {
    const {
        values: { contacts },
    } = useFormikContext<{ contacts: typeof storeContactTable }>();

    const getEmptyContact = () => ({
        id: nanoid(),
        contact: '',
        phone: '',
        email: '',
    });

    return (
        <>
            <h2 css={{ ...typography('h1'), margin: `${scale(2)}px 0` }}>Контактные лица</h2>
            <FieldArray
                name="contacts"
                render={({ push, remove }) => (
                    <>
                        <Block css={{ maxWidth: scale(128) }}>
                            <Block.Body>
                                <table width="100%" css={{ borderCollapse: 'collapse' }}>
                                    <tbody>
                                        <tr>
                                            {columnsContact.map(item => (
                                                <th
                                                    css={{ ...typography('bodySmBold'), ...lineCSS, textAlign: 'left' }}
                                                    key={item.accessor}
                                                >
                                                    {item.Header}
                                                </th>
                                            ))}
                                            <th css={lineCSS} />
                                        </tr>
                                        {contacts.map((c, index: number) => (
                                            <tr key={contacts[index].id}>
                                                <td css={lineCSS}>
                                                    <Form.FastField name={`contacts[${index}].contact`} />
                                                </td>
                                                <td css={lineCSS}>
                                                    <Form.FastField name={`contacts[${index}].phone`}>
                                                        <Mask mask={maskPhone} />
                                                    </Form.FastField>
                                                </td>
                                                <td css={lineCSS}>
                                                    <Form.FastField name={`contacts[${index}].email`} />
                                                </td>
                                                <td css={lineCSS}>
                                                    <Button
                                                        size="sm"
                                                        theme="ghost"
                                                        onClick={() => remove(index)}
                                                        Icon={TrashIcon}
                                                        hidden
                                                    >
                                                        Удалить
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                                <div css={{ display: 'flex', justifyContent: 'center', marginTop: scale(2) }}>
                                    <Button
                                        Icon={PlusIcon}
                                        size="sm"
                                        theme="outline"
                                        type="button"
                                        onClick={() => push(getEmptyContact())}
                                    >
                                        Добавить
                                    </Button>
                                </div>
                            </Block.Body>
                        </Block>
                    </>
                )}
            />
        </>
    );
};

const WorkSchedule = ({ lineCSS }: { lineCSS: CSSObject }) => {
    const {
        values: { schedule },
    } = useFormikContext<{ schedule: typeof storeShipmentTable }>();

    return (
        <>
            <h2 css={{ ...typography('h2'), margin: `${scale(2)}px 0` }}>График работы</h2>
            <Block css={{ maxWidth: scale(128) }}>
                <Block.Body>
                    <table width="100%" css={{ borderCollapse: 'collapse' }}>
                        <tbody>
                            <tr>
                                {columnsschedule.map(({ accessor, Header }) => (
                                    <TableHead key={accessor} Header={Header} lineCSS={lineCSS} />
                                ))}
                            </tr>
                            {schedule.map((item, i: number) => (
                                <tr key={item.id}>
                                    <td css={{ width: '10%', ...lineCSS }}>
                                        <Form.FastField name={`schedule[${i}].check`}>
                                            <Checkbox />
                                        </Form.FastField>
                                    </td>
                                    <td css={lineCSS}>{item.day}</td>
                                    <td css={{ width: '60%', ...lineCSS }}>
                                        <Form.FastField name={`schedule[${i}].time`}>
                                            <DatepickerRange
                                                dateFormat="HH:mm"
                                                showTimeSelect
                                                showTimeSelectOnly
                                                timeIntervals={15}
                                                timeCaption="time"
                                            />
                                        </Form.FastField>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </Block.Body>
            </Block>
        </>
    );
};

const ShipmentSchedule = ({ lineCSS }: { lineCSS: CSSObject }) => {
    const {
        values: { shipment: shipments },
    } = useFormikContext();
    return (
        <>
            <h2 css={{ ...typography('h2'), margin: `${scale(2)}px 0` }}>График отгрузки</h2>
            <Block>
                <Block.Body>
                    <table width="100%" css={{ borderCollapse: 'collapse' }}>
                        <tbody>
                            <tr>
                                {columnsShipment.map(({ Header, accessor }) => (
                                    <TableHead key={accessor} Header={Header} lineCSS={lineCSS} />
                                ))}
                            </tr>
                            {shipments.map((shipment: any, index: number) => (
                                <tr key={index}>
                                    {columnsShipment.map((column, i) => {
                                        if (column.type === 'string') {
                                            return (
                                                <th key={i} css={lineCSS}>
                                                    {shipment.day}
                                                </th>
                                            );
                                        }
                                        if (column.type === 'time') {
                                            return (
                                                <th key={i} css={lineCSS}>
                                                    <Form.FastField name={`shipment[${index}].all`}>
                                                        <Legend label="Создание заявки" />
                                                        <Datepicker
                                                            dateFormat="HH:mm"
                                                            showTimeSelect
                                                            showTimeSelectOnly
                                                            timeIntervals={15}
                                                            timeCaption="time"
                                                        />
                                                    </Form.FastField>
                                                </th>
                                            );
                                        }
                                        if (column.type === 'interval') {
                                            return (
                                                <th key={i} css={lineCSS}>
                                                    <Form.FastField
                                                        name={`shipment[${index}].${column.accessor}.create`}
                                                    >
                                                        <Legend label="Создание заявки" />
                                                        <Datepicker
                                                            dateFormat="HH:mm"
                                                            showTimeSelect
                                                            showTimeSelectOnly
                                                            timeIntervals={15}
                                                            timeCaption="time"
                                                        />
                                                    </Form.FastField>
                                                    <Form.FastField
                                                        name={`shipment.${index}.${column.accessor}.shipment`}
                                                        label="Отгрузка товара"
                                                        css={{ marginTop: scale(1) }}
                                                    >
                                                        <DatepickerRange
                                                            dateFormat="HH:mm"
                                                            showTimeSelect
                                                            showTimeSelectOnly
                                                            timeIntervals={15}
                                                            timeCaption="time"
                                                        />
                                                    </Form.FastField>
                                                </th>
                                            );
                                        }
                                    })}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </Block.Body>
            </Block>
        </>
    );
};

const SellerStoresCreate = () => {
    const { colors } = useTheme();
    const { id } = useParams<{ id: string }>();

    const lineCSS: CSSObject = {
        padding: `${scale(2)}px`,
        borderBottom: `1px solid ${colors?.grey400}`,
        verticalAlign: 'top',
    };

    return (
        <PageWrapper h1={`Редактирование склада ${id}`}>
            <>
                <DatepickerStyles />

                <Form
                    initialValues={{
                        store,
                        schedule: storeScheduleTable,
                        shipment: storeShipmentTable,
                        contacts: storeContactTable,
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <div css={{ display: 'flex' }}>
                        <div>
                            <StoreEdit />

                            <WorkSchedule lineCSS={lineCSS} />

                            <ShipmentSchedule lineCSS={lineCSS} />

                            <Contacts lineCSS={lineCSS} />
                        </div>
                        <div css={{ position: 'relative', marginLeft: scale(2) }}>
                            <Button size="sm" theme="primary" css={{ position: 'sticky', top: scale(3) }} type="submit">
                                Сохранить
                            </Button>
                        </div>
                    </div>
                </Form>
            </>
        </PageWrapper>
    );
};

export default SellerStoresCreate;
