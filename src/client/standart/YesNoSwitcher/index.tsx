import React from 'react';
import { FieldInputProps } from 'formik';
import Switcher from '@standart/Switcher';

const YesNoSwitcher = ({ name = '', field, ...props }: { name?: string; field?: FieldInputProps<any> }) => {
    return (
        <Switcher name={name} field={field} checked={field?.value} {...props}>
            {field?.value ? 'Да' : 'Нет'}
        </Switcher>
    );
};

export default YesNoSwitcher;
