import React from 'react';
import { FieldInputProps, FieldMetaProps } from 'formik';
import Legend from '@standart/Legend';
import { scale } from '@greensight/gds';
import useFieldCSS from '@scripts/useFieldCSS';
import { FormFieldProps } from './Field';
export interface BasicFieldProps extends FormFieldProps {
    field?: FieldInputProps<any>;
    meta?: FieldMetaProps<any>;
}

export const BasicField = ({
    name,
    label,
    success,
    isLegend = false,
    hint,
    Icon,
    iconCss,
    field,
    meta,
    ...props
}: BasicFieldProps) => {
    const { basicFieldCSS } = useFieldCSS(meta);
    return (
        <>
            {((isLegend && meta) || label) && (
                <Legend
                    name={name}
                    meta={!props?.disabled ? meta : undefined}
                    label={label}
                    success={success}
                    hint={hint}
                />
            )}
            <div css={{ position: 'relative' }}>
                {Icon && (
                    <Icon
                        css={{
                            position: 'absolute',
                            left: scale(1),
                            top: '50%',
                            transform: 'translateY(-50%)',
                            ...iconCss,
                        }}
                    />
                )}
                <input
                    id={name}
                    {...field}
                    {...props}
                    css={{ ...basicFieldCSS, ...(Icon && { paddingLeft: scale(4) }) }}
                />
            </div>
        </>
    );
};

export default BasicField;
