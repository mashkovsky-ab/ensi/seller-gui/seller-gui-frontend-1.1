import React, { useEffect, useRef } from 'react';
import { useTheme, scale } from '@greensight/gds';
import { FieldInputProps, FieldMetaProps, FieldHelperProps } from 'formik';
import { CSSObject } from '@emotion/core';

export interface SwitcherProps extends React.HTMLProps<HTMLInputElement> {
    /** Formik field object (inner) */
    field?: FieldInputProps<string[]>;
    /** Active state indeterminate */
    isIndeterminate?: boolean;
    /** Are all values selected for indeterminate state */
    all?: boolean;
    /** Checkbox text with indeterminate state */
    indeterminate?: string;
    /** Ref for inner input */
    innerRef?: React.Ref<HTMLInputElement>;
    /** Additional class for label */
    labelClass?: string;
    /** Additional class */
    className?: string;
    /** Formik meta object (inner) */
    meta?: FieldMetaProps<any>;
    /** Formik helpers (inner) */
    helpers?: FieldHelperProps<any>;
    /** Switcher description */
    description?: string;
    css?: CSSObject;
}

const Switcher = ({
    name,
    field,
    value,
    isIndeterminate = false,
    all = false,
    indeterminate,
    innerRef,
    children,
    className,
    description,
    css,
    ...props
}: SwitcherProps) => {
    delete props.helpers;
    delete props.meta;
    const { colors } = useTheme();
    const id = `${name}-${value}`;

    const _ref = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (!indeterminate) return;
        if (_ref.current) _ref.current.indeterminate = isIndeterminate;
        if (_ref.current) _ref.current.checked = all;
    }, [_ref, all, indeterminate, isIndeterminate]);
    const checked = field
        ? typeof value === 'string'
            ? field?.value?.includes(value)
            : typeof field?.value === 'boolean'
            ? field.value
            : undefined
        : undefined;
    return (
        <div className={className} css={css}>
            <input
                checked={checked}
                {...props}
                {...field}
                value={value}
                name={name}
                id={id}
                type="checkbox"
                ref={innerRef || _ref}
                css={{
                    height: 0,
                    width: 0,
                    clip: 'rect(0 0 0 0)',
                    position: 'absolute',
                    '&:focus': {
                        outline: `none`,
                    },
                }}
            />
            <label
                htmlFor={id}
                css={{
                    display: 'flex',
                    position: 'relative',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingLeft: scale(7),
                    cursor: 'pointer',
                    color: colors?.grey900,
                    fontSize: 14,
                    lineHeight: 1.3,
                    zIndex: 0,

                    'input:disabled + &': {
                        color: colors?.grey600,
                    },
                    'input:focus + &': {
                        outline: `2px solid ${colors?.grey900}`,
                        outlineOffset: '4px',
                    },
                    '.js-focus-visible input:focus:not(.focus-visible) + &': {
                        outline: 'none',
                    },

                    '&::after': {
                        content: '""',
                        position: 'absolute',
                        top: 'calc(50% - 12px)',
                        left: 0,
                        display: 'block',
                        width: scale(6),
                        height: scale(3),
                        background: colors?.grey300,
                        borderRadius: '100px',
                        transition: 'background-color .2s',
                        zIndex: 1,
                        'input:checked + &': {
                            backgroundColor: colors?.primary,
                        },
                        'input:disabled + &': {
                            backgroundColor: colors?.grey200,
                            border: '1px solid #CDD2D7',
                            cursor: 'not-allowed',
                        },
                    },

                    '&::before': {
                        content: '""',
                        display: 'block',
                        position: 'absolute',
                        top: 'calc(50% - 10px)',
                        left: '2px',
                        width: '20px',
                        height: '20px',
                        borderRadius: '100%',
                        transition: '.2s',
                        background: colors?.white,
                        boxShadow: '1px 1px 2px rgba(38, 41, 43, 0.2), 0px 1px 5px rgba(38, 41, 43, 0.2)',
                        zIndex: 2,
                        'input:checked + &': {
                            left: 'calc(46px)',
                            transform: 'translateX(-100%)',
                        },
                        'input:active:not(:disabled) + &': {
                            width: '24px',
                        },
                        'input:disabled + &': {
                            background: colors?.grey400,
                            cursor: 'not-allowed',
                        },
                    },
                }}
            >
                {children}
            </label>
            {description && (
                <span css={{ display: 'block', marginTop: scale(1), color: colors?.grey800 }}>{description}</span>
            )}
        </div>
    );
};

export default Switcher;
