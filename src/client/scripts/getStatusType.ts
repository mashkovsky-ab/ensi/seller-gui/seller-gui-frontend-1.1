import { STATUSES } from './enums';

export default (status: string) => {
    let type: STATUSES;
    switch (status.toLowerCase().trim()) {
        case 'в продаже':
        case 'активен':
        case 'согласовано':
        case 'обработана':
        case 'сохранено':
        case 'доступен к продаже': {
            type = STATUSES.SUCCESS;
            break;
        }
        case 'недоступен к продаже':
        case 'закрытая': {
            type = STATUSES.ERROR;
            break;
        }
        case 'новая':
        case 'приостановлен':
        case 'снято с продажи': {
            type = STATUSES.WARNING;
            break;
        }
        default: {
            type = STATUSES.REGULAR;
        }
    }

    return type;
};
