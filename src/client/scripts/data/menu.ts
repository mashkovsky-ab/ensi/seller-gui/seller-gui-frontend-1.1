import menuIcons from '@scripts/menuIcons';
import { nanoid } from 'nanoid';
import { MenuItemProps } from '@components/Sidebar';

const menu: MenuItemProps[] = [
    {
        text: 'Заказы',
        Icon: menuIcons.cart,
        subMenu: [
            { text: 'Заказы на сборку', link: '/shipment/list' },
            { text: 'Проблемные заказы', link: '/shipment/problem' },
            { text: 'Грузы', link: '/shipment/cargo' },
        ],
    },
    {
        text: 'Заявки',
        Icon: menuIcons.trello,
        subMenu: [
            { text: 'Проверка товаров', link: '/requests/check' },
            { text: 'Производство  контента', link: '/requests/content' },
            { text: 'Изменение  цен', link: '/requests/price-change' },
        ],
    },
    {
        text: 'Каталог',
        Icon: menuIcons.folders,
        subMenu: [
            {
                text: 'Товары',
                subMenu: [
                    { text: 'В продаже', link: '/catalog/products' },
                    { text: 'В продакшн', link: '/catalog/production' },
                    { text: 'На модерации', link: '/catalog/moderation' },
                    { text: 'Архив', link: '/catalog/archive' },
                ],
            },
            { text: 'Товарные группы', link: '/catalog/products/variant-groups' },
            {
                text: 'Импорт',
                subMenu: [
                    { text: 'Импорт товаров', link: '/products/products-import' },
                    { text: 'Импорт остатков', link: '/products/amounts-import' },
                    { text: 'Импорт цен', link: '/products/prices-import' },
                    { text: 'Задачи на импорт', link: '/products/imports' },
                    {
                        text: 'Справочники',
                        subMenu: [
                            { text: 'Бренды', link: '/catalog/brands' },
                            { text: 'Категории', link: '/catalog/categories' },
                        ],
                    },
                ],
            },
        ],
    },
    {
        text: 'Маркетинг',
        Icon: menuIcons.birka,
        subMenu: [
            { text: 'Скидки', link: '/marketing/discounts' },
            { text: 'Бандлы', link: '/marketing/bundles' },
            { text: 'Промокоды', link: '/marketing/promo-code' },
        ],
    },
    {
        text: 'Аналитика',
        Icon: menuIcons.sliders,
        subMenu: [{ text: 'Дашборд', link: '/reports/dashboard' }],
    },
    {
        text: 'Продавец',
        Icon: menuIcons.shop,
        subMenu: [
            { text: 'Карточка организации', link: '/seller' },
            { text: 'Менеджеры', link: '/seller/managers' },
            { text: 'Склады', link: '/stores/seller-stores' },
        ],
    },
    {
        text: 'Коммуникации',
        Icon: menuIcons.message,
        subMenu: [{ text: 'Список чатов', link: '/communications/chats' }],
    },
];

const enrichMenu = (menu: MenuItemProps[]) => {
    menu.forEach((item: any) => {
        item.id = nanoid(5);
        if (item.subMenu) enrichMenu(item.subMenu);
    });
};
enrichMenu(menu);

export default menu;
