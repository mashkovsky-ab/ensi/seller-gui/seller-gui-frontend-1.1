import PackageIcon from '@svg/tokens/small/package.svg';
import CartIcon from '@svg/tokens/small/cart.svg';
import TrelloIcon from '@svg/tokens/small/trello.svg';
import ImageIcon from '@svg/tokens/small/image.svg';
import TruckIcon from '@svg/tokens/small/truck.svg';
import UsersIcon from '@svg/tokens/small/users.svg';
import GlobeIcon from '@svg/tokens/small/globe.svg';
import AwardIcon from '@svg/tokens/small/award.svg';
import BirkaIcon from '@svg/tokens/small/birka.svg';
import MessageIcon from '@svg/tokens/small/message.svg';
import SettingsIcon from '@svg/tokens/small/settings.svg';
import ChartIcon from '@svg/tokens/small/chart.svg';
import Warehouse from '@svg/tokens/small/warehouse.svg';
import Import from '@svg/tokens/small/import.svg';
import Book from '@svg/tokens/small/book.svg';
import ShopIcon from '@svg/tokens/small/shop.svg';
import Sliders from '@svg/tokens/small/sliders.svg';
import Folders from '@svg/tokens/small/folders.svg';

const menuIcons = {
    package: PackageIcon,
    cart: CartIcon,
    trello: TrelloIcon,
    image: ImageIcon,
    truck: TruckIcon,
    users: UsersIcon,
    globe: GlobeIcon,
    award: AwardIcon,
    birka: BirkaIcon,
    shop: ShopIcon,
    message: MessageIcon,
    settings: SettingsIcon,
    chart: ChartIcon,
    warehouse: Warehouse,
    import: Import,
    book: Book,
    sliders: Sliders,
    folders: Folders,
};

export default menuIcons;
