export interface Subcategory {
    id: number;
    code: string;
    name: string;
}

export interface Category {
    id: number;
    code: string;
    name: string;
    items: Subcategory[];
}

export async function getCategory(categoryCode: string) {
    const response = await mockCategory(categoryCode);
    return response;
}

const mockCategory = async (categoryCode: string) => {
    let mockData: Category | null = null;
    if (categoryCode === 'category-1') {
        mockData = {
            id: 1,
            code: 'category-1',
            name: 'Категория 1',
            items: [
                { id: 1, code: 'subcategory-1', name: 'Подкатегория 1' },
                { id: 2, code: 'subcategory-2', name: 'Подкатегория 2' },
                { id: 3, code: 'subcategory-3', name: 'Подкатегория 3' },
            ],
        };
    }
    const response = await Promise.resolve({ data: mockData });
    return response;
};
