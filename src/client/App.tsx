import { hot } from 'react-hot-loader/root';
import React, { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { useTheme } from '@greensight/gds';
import { ErrorBoundary } from 'react-error-boundary';
import ErrorFallback from '@components/ErrorFallback';
import Sidebar from '@containers/Sidebar';
import routes from './pages';
import Auth from '@components/Auth';
import Header from '@components/Header';
import { useDispatch, useSelector } from 'react-redux';
import { selectors as userSelectors, setProfile } from '@reducers/profile';
import { useCookies } from 'react-cookie';

const App = () => {
    const { colors } = useTheme();
    const [overlay, setOverlay] = useState(false);
    const dispatch = useDispatch();
    const user = useSelector(userSelectors.user);
    const [, setCookie, removeCookie] = useCookies(['token']);

    /** обновим куку в при обновлении токена */
    useEffect(() => {
        if (user) {
            setCookie('token', user, { maxAge: 60 * 60 * 60 * 24, path: '/' });
        } else {
            removeCookie('token', { maxAge: -1, path: '/' });
        }
    }, [removeCookie, setCookie, user]);

    return (
        <div
            css={{
                backgroundColor: colors?.grey200,
                minHeight: '100vh',
                display: 'grid',
                placeItems: 'center',
                ...(overlay && {
                    '&::after': {
                        content: '""',
                        position: 'fixed',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        background: colors?.black,
                        opacity: 0.3,
                    },
                }),
            }}
        >
            {user ? (
                <>
                    <div css={{ display: 'flex', width: '100%', height: '100%' }}>
                        <Sidebar setOverlay={setOverlay} />
                        <div
                            css={{
                                flexGrow: 1,
                                flexShrink: 1,
                                // если добавить, сломается position: sticky
                                //  overflowX: 'hidden'
                            }}
                        >
                            <Header
                                onLogout={() => {
                                    dispatch(setProfile(null));
                                }}
                            />
                            <Switch>
                                {routes.map(({ page: Page, status, ...route }) => (
                                    <Route
                                        key={typeof route.path === 'object' ? route.path[0] : route.path}
                                        render={({ staticContext }: any) => {
                                            if (status && staticContext) staticContext.status = status;
                                            return (
                                                <ErrorBoundary FallbackComponent={ErrorFallback}>
                                                    <Page />
                                                </ErrorBoundary>
                                            );
                                        }}
                                        {...route}
                                    />
                                ))}
                            </Switch>
                        </div>
                    </div>
                </>
            ) : (
                <Auth
                    onSubmit={() => {
                        dispatch(setProfile('login'));
                    }}
                />
            )}
        </div>
    );
};

export default hot(App);
