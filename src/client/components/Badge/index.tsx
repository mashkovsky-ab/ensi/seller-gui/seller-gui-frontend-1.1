import React from 'react';
import { useTheme, scale } from '@greensight/gds';
import typography from '@scripts/typography';
import { STATUSES } from '@scripts/enums';
import getStatusType from '@scripts/getStatusType';
export interface BadgeProps {
    text: string;
    type?: STATUSES;
}

const Badge = ({ text, type }: BadgeProps) => {
    const { colors } = useTheme();
    let backgroundColor;

    if (!type) type = getStatusType(text);

    switch (type) {
        case STATUSES.SUCCESS: {
            backgroundColor = colors?.success;
            break;
        }
        case STATUSES.ERROR: {
            backgroundColor = colors?.danger;
            break;
        }
        case STATUSES.WARNING: {
            backgroundColor = colors?.warning;
            break;
        }
        default: {
            backgroundColor = colors?.secondaryHover;
        }
    }

    return (
        <div
            css={{
                display: 'inline-flex',
                alignItems: 'center',
                backgroundColor,
                color: colors?.white,
                borderRadius: 2,
                padding: `1px ${scale(1, true)}px`,
                whiteSpace: 'nowrap',
                ...typography('smallBold'),
            }}
        >
            {text}
        </div>
    );
};

export default Badge;
