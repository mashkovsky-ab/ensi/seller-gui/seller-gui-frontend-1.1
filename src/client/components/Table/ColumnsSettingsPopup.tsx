import React, { useRef } from 'react';
import { scale } from '@greensight/gds';
import Checkbox, { CheckboxProps } from '@standart/Checkbox';
import Popup from '@standart/Popup';

import IndeterminateCheckbox from './IndeterminateCheckbox';

const RefCheckbox = (props: CheckboxProps) => {
    const ref = useRef<HTMLInputElement>(null);
    return <Checkbox innerRef={ref} {...props}></Checkbox>;
};

interface ColumnsSettingsPopupProps {
    isOpen: boolean;
    close: () => void;
    getToggleHideAllColumnsProps: any;
    allColumns: { Header?: any; getToggleHiddenProps: () => void; id: string }[];
}

const ColumnsSettingsPopup = ({
    isOpen,
    close,
    getToggleHideAllColumnsProps,
    allColumns,
}: ColumnsSettingsPopupProps) => {
    return (
        <Popup isOpen={isOpen} onRequestClose={close} popupCss={{ minWidth: scale(50) }} title="Настройка столбцов">
            <IndeterminateCheckbox {...getToggleHideAllColumnsProps()} id="settings" css={{ marginBottom: scale(1) }}>
                Toggle All
            </IndeterminateCheckbox>
            {allColumns.map(column => {
                return (
                    <RefCheckbox
                        key={column.id}
                        name={column.id}
                        value={column.id}
                        {...column.getToggleHiddenProps()}
                        css={{ marginBottom: scale(1) }}
                    >
                        {column.Header}
                    </RefCheckbox>
                );
            })}
        </Popup>
    );
};

export default ColumnsSettingsPopup;
