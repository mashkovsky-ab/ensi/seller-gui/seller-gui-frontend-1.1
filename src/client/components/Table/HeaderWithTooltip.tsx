import React from 'react';
import { scale } from '@greensight/gds';
import Tooltip from '@standart/Tooltip';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';

interface HeaderWithTooltipProps {
    headerText: string;
    tooltipText: string;
    tooltipPlacement?: any;
}

const headerWithTooltip = ({ headerText, tooltipText, tooltipPlacement = 'right' }: HeaderWithTooltipProps) => (
    <>
        {headerText}
        <Tooltip content={tooltipText} arrow={true} placement={tooltipPlacement}>
            <button css={{ verticalAlign: 'bottom', marginLeft: scale(1, true), height: scale(2) }}>
                <TipIcon />
            </button>
        </Tooltip>
    </>
);

export default headerWithTooltip;
