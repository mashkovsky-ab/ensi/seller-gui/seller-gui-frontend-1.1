import React from 'react';
import { scale, useTheme } from '@greensight/gds';

export interface BlockFooterProps {
    className?: string;
    children?: React.ReactNode;
}

const BlockFooter = ({ className, children }: BlockFooterProps) => {
    const { colors } = useTheme();

    return (
        <div
            className={className}
            css={{
                borderTop: `1px solid ${colors?.grey400}`,
                padding: `${scale(2)}px ${scale(3)}px`,
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                minHeight: scale(8),
            }}
        >
            {children}
        </div>
    );
};

export default BlockFooter;
