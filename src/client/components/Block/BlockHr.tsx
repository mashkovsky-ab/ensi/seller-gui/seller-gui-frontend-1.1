import React from 'react';
import { useTheme, scale } from '@greensight/gds';

export interface BlockHrProps {
    className?: string;
}

const BlockHr = ({ className }: BlockHrProps) => {
    const { colors } = useTheme();

    return (
        <hr
            className={className}
            css={{
                borderTop: `1px solid ${colors?.grey400}`,
                margin: `${scale(1)}px 0`,
            }}
        />
    );
};

export default BlockHr;
