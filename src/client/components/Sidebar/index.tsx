import React, { useState, useRef, useCallback, useMemo } from 'react';
import { useTheme, scale } from '@greensight/gds';
import { Link, matchPath, useLocation } from 'react-router-dom';
import { CSSObject } from '@emotion/core';
import useOnClickOutside from '@scripts/useOnClickOutside';
import CheckIcon from '@svg/tokens/small/check.svg';
import ChevronRightIcon from '@svg/tokens/small/chevronRight.svg';
import ChevronLeftIcon from '@svg/tokens/small/chevronLeft.svg';
import typography from '@scripts/typography';

enum MENU_TYPES {
    // 1 уровень меню
    MAIN = 'main',
    // 2 уровень меню
    SUBMENU = 'submenu',
    // 3 уровень меню
    STATUSES = 'statuses',
    // 4 уровень меню
    FOURTH = 'fourth',
}

enum TRIGGERS {
    HOVER = 'hover',
    CLICK = 'click',
}

enum MENU_WIDTH {
    MAIN = scale(30),
    MAIN_CUT = scale(8),
    SUBMENU = scale(32),
}

export interface MenuItemProps {
    id?: string;
    text: string;
    link?: string;
    subMenu?: MenuItemProps[];
    Icon?: SVGRIcon;
}

export interface SidebarProps {
    menuItems: MenuItemProps[];
    isCutDown: boolean;
    cutDownHandler: () => void;
    setOverlay: (overlay: boolean) => void;
}

const getMainMenuWidth = (isCutDown: boolean) => (!isCutDown ? MENU_WIDTH.MAIN : MENU_WIDTH.MAIN_CUT);

const Sidebar = ({ menuItems = [], isCutDown, cutDownHandler, setOverlay, ...props }: SidebarProps) => {
    const { colors } = useTheme();
    const [menuItem, setMenuItem] = useState<MenuItemProps | null>(null);
    const [subItem, setSubItem] = useState<MenuItemProps | null>(null);
    const [deepItem, setDeepItem] = useState<MenuItemProps | null>(null);
    const menuRef = useRef<HTMLDivElement | null>(null);
    const location = useLocation();

    const getMatch = useCallback((link?: string) => link && Boolean(matchPath(link, location.pathname)?.path), [
        location.pathname,
    ]);

    const activeMenuPoints = useMemo(() => {
        const activeMenuPoints: MenuItemProps[] = [];
        const findActive = (items?: MenuItemProps[]): MenuItemProps | undefined => {
            if (!items) return undefined;
            const finded = items.find(i => {
                if (i.link) return getMatch(i.link);
                return findActive(i.subMenu);
            });
            if (finded) activeMenuPoints.push(finded);
            return finded;
        };
        findActive(menuItems);
        return activeMenuPoints;
    }, [getMatch, menuItems]);

    const closeAllMenu = () => {
        setMenuItem(null);
        setSubItem(null);
        setDeepItem(null);
    };

    useOnClickOutside(menuRef, () => {
        closeAllMenu();
        setOverlay(false);
    });

    const menuItemStyle: CSSObject = {
        display: 'flex',
        alignItems: 'center',
        color: colors?.grey600,
        fill: 'currentColor',
        width: '100%',
        padding: `${scale(3, true)}px ${scale(3)}px`,
        textAlign: 'left',
        background: 'transparent',
        '&:hover': {
            color: colors?.white,
            backgroundColor: colors?.dark,
        },
    };

    const subMenuStyle = (type: string) =>
        ({
            width: MENU_WIDTH.SUBMENU,
            backgroundColor: colors?.white,
            borderLeft: `1px solid ${colors?.lightBlue}`,
            position: 'absolute',
            height: '100%',
            left:
                getMainMenuWidth(isCutDown) +
                (type === MENU_TYPES.STATUSES ? MENU_WIDTH.SUBMENU : 0) +
                (type === MENU_TYPES.FOURTH ? MENU_WIDTH.SUBMENU * 2 : 0),
        } as CSSObject);

    const getMenuItemContent = (menuItem: MenuItemProps, type: string, match: boolean) => (
        <>
            {menuItem.Icon && <menuItem.Icon css={{ marginRight: scale(2), flexShrink: 0 }} />}
            {(!isCutDown || type !== MENU_TYPES.MAIN) && (
                <>
                    <span css={{ marginRight: scale(2) }}>{menuItem.text}</span>
                    {(type === MENU_TYPES.STATUSES || type === MENU_TYPES.FOURTH) && menuItem.link && match && (
                        <CheckIcon css={{ marginLeft: 'auto', flexShrink: 0 }} />
                    )}
                    {menuItem.subMenu && menuItem.subMenu.length > 0 && (
                        <ChevronRightIcon css={{ marginLeft: 'auto', flexShrink: 0 }} />
                    )}
                </>
            )}
        </>
    );

    const getListElement = (
        listItem: MenuItemProps,
        setItem: ((item: MenuItemProps | null) => void) | null,
        type: string,
        trigger = TRIGGERS.CLICK
    ) => {
        const lightTheme = type === MENU_TYPES.STATUSES || type === MENU_TYPES.SUBMENU || type === MENU_TYPES.FOURTH;

        const match = Boolean(activeMenuPoints.find(i => i.id === listItem.id));

        const itemStyle: CSSObject = {
            ...menuItemStyle,
            ...typography('bodySm'),
            '&:disabled': {
                cursor: 'default',
                color: colors?.grey600,
            },
            ...(lightTheme && {
                color: colors?.black,
                '&:hover': {
                    color: colors?.primary,
                    background: colors?.grey100,
                },
            }),
            ...(match && {
                color: lightTheme ? colors?.primary : colors?.white,
            }),
        };

        const isNotActiveAlready =
            (menuItem && listItem.id === menuItem.id && type === MENU_TYPES.MAIN) ||
            (subItem && listItem.id === subItem.id && type === MENU_TYPES.SUBMENU) ||
            (deepItem && listItem.id === deepItem.id && type === MENU_TYPES.STATUSES);
        const isActiveBtn = !!(listItem.subMenu && !isNotActiveAlready);

        const menuBtnTrigger = () => {
            if (isActiveBtn) {
                if (type === MENU_TYPES.MAIN) {
                    closeAllMenu();
                }

                if (setItem) setItem(listItem);
            } else {
                if (type === MENU_TYPES.SUBMENU || type === MENU_TYPES.STATUSES) {
                    if (setItem) setItem(null);
                } else {
                    closeAllMenu();
                }
            }

            if (setOverlay && type === MENU_TYPES.MAIN) setOverlay(isActiveBtn);
        };

        const menuLinkTrigger = () => {
            if (type === MENU_TYPES.STATUSES && setItem) {
                setItem(null);
            } else {
                closeAllMenu();
            }
            if (setOverlay) setOverlay(false);
        };

        const isDisableItem = !listItem.subMenu || listItem.subMenu.length === 0;

        return (
            <>
                {listItem.link && isDisableItem ? (
                    <Link css={itemStyle} to={listItem.link} onClick={menuLinkTrigger} title={listItem?.text}>
                        {getMenuItemContent(listItem, type, match)}
                    </Link>
                ) : (
                    <button
                        css={itemStyle}
                        disabled={isDisableItem}
                        onClick={menuBtnTrigger}
                        title={listItem?.text}
                        onMouseEnter={trigger === TRIGGERS.HOVER && isActiveBtn ? menuBtnTrigger : undefined}
                    >
                        {getMenuItemContent(listItem, type, match)}
                    </button>
                )}
            </>
        );
    };

    const getMenuLvl = (
        menuItem: MenuItemProps,
        setSubItem: ((item: MenuItemProps | null) => void) | null,
        menuType: string,
        trigger = TRIGGERS.CLICK
    ) =>
        menuItem && (
            <div css={subMenuStyle(menuType)}>
                <ul css={{ position: 'sticky', top: 0 }}>
                    {menuItem?.subMenu?.map(subMenuItem => (
                        <li key={subMenuItem.id}>{getListElement(subMenuItem, setSubItem, menuType, trigger)}</li>
                    ))}
                </ul>
            </div>
        );

    return (
        <aside
            css={{
                display: 'flex',
                position: 'relative',
                zIndex: 1,
            }}
            ref={menuRef}
            {...props}
        >
            <div
                css={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    width: getMainMenuWidth(isCutDown),
                    height: '100%',
                    backgroundColor: colors?.secondaryHover,
                    color: colors?.white,
                }}
            >
                <ul css={{ overflow: 'auto', position: 'sticky', top: 0, paddingBottom: scale(6) }}>
                    {menuItems.map(menuItem => (
                        <li key={menuItem.id}>{getListElement(menuItem, setMenuItem, MENU_TYPES.MAIN)}</li>
                    ))}
                </ul>
                <button
                    css={{
                        position: 'sticky',
                        bottom: 0,
                        zIndex: 1,
                        ...menuItemStyle,
                        backgroundColor: colors?.secondaryHover,
                        ...(isCutDown && {
                            justifyContent: 'center',
                        }),
                        padding: `${scale(2)}px ${scale(3)}px`,
                    }}
                    onClick={cutDownHandler}
                >
                    {!isCutDown ? (
                        <>
                            <ChevronLeftIcon css={{ marginRight: scale(1) }} />
                            Свернуть
                        </>
                    ) : (
                        <ChevronRightIcon />
                    )}
                </button>
            </div>
            {menuItem && getMenuLvl(menuItem, setSubItem, MENU_TYPES.SUBMENU, TRIGGERS.HOVER)}
            {menuItem && subItem && getMenuLvl(subItem, setDeepItem, MENU_TYPES.STATUSES)}
            {menuItem && subItem && deepItem && getMenuLvl(deepItem, null, MENU_TYPES.FOURTH)}
        </aside>
    );
};

export default Sidebar;
