import React from 'react';
import Form from '@standart/Form';
import { FormikValues } from 'formik';
import Password from '@standart/Password';
import typography from '@scripts/typography';
import { scale, useTheme, Button } from '@greensight/gds';
import * as Yup from 'yup';

interface AuthProps {
    onSubmit: (values: FormikValues) => void;
}

const Auth = ({ onSubmit }: AuthProps) => {
    const { shadows, colors } = useTheme();
    return (
        <div
            css={{
                width: scale(45),
                boxShadow: shadows?.big,
                padding: `${scale(3)}px ${scale(4)}px`,
                backgroundColor: colors?.white,
                marginLeft: 'auto',
                marginRight: 'auto',
            }}
        >
            <h1 css={{ ...typography('h1'), textAlign: 'center', marginTop: 0 }}>Авторизация</h1>
            <Form
                onSubmit={onSubmit}
                initialValues={{ login: '', password: '' }}
                validationSchema={Yup.object().shape({
                    login: Yup.string().email('Некорректный адрес почты').required('Введите почту'),
                    password: Yup.string().min(8, 'Минимум 8 символов').required('Введите пароль'),
                })}
            >
                <Form.FastField name="login" label="E-mail" css={{ marginBottom: scale(2) }} />
                <Form.FastField name="password" label="Пароль" css={{ marginBottom: scale(4) }}>
                    <Password />
                </Form.FastField>
                <Button type="submit" size="sm" block>
                    Войти
                </Button>
            </Form>
        </div>
    );
};

export default Auth;
