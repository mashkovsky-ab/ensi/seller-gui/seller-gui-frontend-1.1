import React from 'react';
import { Editor as TinyMCEEditor } from '@tinymce/tinymce-react';
import { FieldProps, FieldHelperProps, FieldMetaProps } from 'formik';

export interface EditorProps {
    field?: FieldProps;
    meta?: FieldMetaProps<string>;
    helpers?: FieldHelperProps<string>;
    onChange?: (str: string) => void;
}

const Editor = ({ helpers, onChange }: EditorProps) => {
    return (
        <>
            <TinyMCEEditor
                // непонятно отчего но апи ключ не прокидывается напрямую
                apiKey={process.env.TINY_MCE_API_KEY}
                onEditorChange={str => {
                    if (helpers) helpers.setValue(str);
                    if (onChange) onChange(str);
                }}
                init={{
                    height: 300,
                    menubar: false,
                    toolbar:
                        'undo redo | formatselect | ' +
                        'bold italic backcolor | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | help',
                    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
                }}
            />
        </>
    );
};

export default Editor;
