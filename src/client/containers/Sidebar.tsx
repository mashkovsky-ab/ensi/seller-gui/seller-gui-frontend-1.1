import React, { useState } from 'react';
import { useCookies } from 'react-cookie';
import mockMenu from '@scripts/data/menu';
import { MAX_AGE_NEVER } from '@scripts/constants';
import Sidebar from '@components/Sidebar';

const SidebarContainer = ({ setOverlay }: { setOverlay: (overlay: boolean) => void }) => {
    const [cookies, setCookie] = useCookies(['isCutDown']);
    const [isCutDown, setIsCutDown] = useState(cookies.isCutDown === 'true' ? true : false);

    return (
        <Sidebar
            menuItems={mockMenu}
            isCutDown={isCutDown}
            cutDownHandler={() => {
                const state = !isCutDown;
                setCookie('isCutDown', state, { maxAge: MAX_AGE_NEVER, path: '/' });
                setIsCutDown(state);
            }}
            setOverlay={setOverlay}
        />
    );
};

export default SidebarContainer;
